<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Formulir Rencana Studi</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/easyui.css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="stylesheet" type="text/css" href="css/color.css">
	<link rel="stylesheet" type="text/css" href="css/demo.css">
	<link rel="stylesheet" type="text/css" href="css/apps.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">

	<script src="jss/jquery.min.js"></script>
	<script src="jss/bootstrap.min.js"></script>
	<script src="jss/bootbox.min.js"></script>
	<script type="text/javascript" src="jss/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="jss/apps.js"></script>
	<script type="text/javascript" src="jss/jquery.dataTables.min.js"></script>
    <script type="text/javascript">

    </script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable( {
				"paging":   false,
				"ordering": false,
				"info":     false
			} );
		} );
	</script>
	<style type="text/css">
		td {
			padding: 0px 2px 0px 2px;
		}
	</style>
</head>
<body>
	<?php include "inc_nav.php"; ?>
	<h2>Formulir Rencana Studi</h2>
    <label>NRP : </label>
    <!--input id="search" name="search" style="width:200px" class="easyui-textbox" maxlength="5"-->
		<input id="search" type="hidden" value="">
		<select id="cc" class="easyui-combogrid" name="cc" style="width:250px;"
        data-options="
            panelWidth:450,
            value:'',
            idField:'nrp',
            textField:'nrp',
            url:'app-frs.php?action=getmhs',
            columns:[[
                {field:'nrp',title:'NRP',width:60},
                {field:'nama',title:'Nama',width:390}
            ]],
						onChange: function(newValue,oldValue){
							$('#search').val(newValue);
						}
        "></select>
    <a href="javascript:void(0)" iconCls="icon-search" id="btn_search" class="easyui-linkbutton">Cari</a>
	<a href="" iconCls="icon-clear" id="btn_clear" class="easyui-linkbutton">Reset</a>
    <div style="height: 30px"></div>
    <div id="panel-mhs" closed="true" class="easyui-panel" title="Data Mahasiswa" style="width:100%;max-width: 880px;padding:20px;">
		<table>
			<tr>
				<td width="160">Nama</td><td width="10" align="center">:</td><td width="250"><div id="dat_nama"></div></td><td width="20"></td>
				<td width="160">Periode</td><td width="10" align="center">:</td><td width="250"><div id="dat_ajaran">
				<select id="periode">
					<option value="1">Gasal</option>
					<option value="2">Genap</option>
				</select>
				<select id="tahun">
					<!--option value="2017">2017/2018</option>
					<option value="2016">2016/2017</option-->
					<?php
						include "conn.php";
						$rs = $conn->query("SELECT DISTINCT tahun FROM ajaran ORDER BY tahun DESC");
						while ($row = $rs->fetch_assoc()) {
							echo "<option value='".$row['tahun']."'>".$row['tahun']."/".($row['tahun']+1)."</option>";
						}
						$conn->close();
					 ?>
				</select>
				<a href="javascript:void(0)" id="btn_ganti" class="easyui-linkbutton">Ganti</a>
				</div></td>
			</tr>
			<tr>
				<td width="160">NRP</td><td width="10" align="center">:</td><td width="250"><div id="dat_nrp"></div></td><td width="20"></td>
				<td width="160">Jumlah SKS Diambil</td><td width="10" align="center">:</td><td width="250"><div id="jml_sks">0</div></td>
			</tr>
			<tr>
				<td width="160">Dosen Wali</td><td width="10" align="center">:</td><td width="250"><div id="dat_wali"></div></td><td width="20"></td>
				<td width="160">Jumlah MK Diambil</td><td width="10" align="center">:</td><td width="250"><div id="jml_mk">0</div></td>
				<!--td width="160">Periode</td><td width="10" align="center">:</td><td width="250"></td-->
			</tr>
		</table>
	</div>
	<div style="height: 30px"></div>
	<div id="panel-matkul" closed="true" class="easyui-panel" style="border: 0px;">
	<form id="frm_matkul" method="post" novalidate>
	<input type="hidden" id="frm_nrp" name="frm_nrp">
	<input type="hidden" id="frm_periode" name="frm_periode">
	<label>Mata Kuliah : </label>
	<input id="nama_matkul" name="nama_matkul" class="easyui-combobox" style="width:100%;max-width:400px;" required="true"
				data-options="valueField:'id',textField:'nama'">
	    <a href="javascript:void(0)" iconCls="icon-ok" onclick="" id="btn_ambil" class="easyui-linkbutton">Ambil</a>
	</form>
	<div style="height: 30px"></div>
	<table id="table_frs" class="easyui-datagrid" style="width:100%;height:auto"
            toolbar="#toolbar" fitColumns="true" data-options="singleSelect:true,url:'',method:'get'">
        <thead>
            <tr>
				<th data-options="field:'id',width:40,hidden:true">ID</th>
                <th data-options="field:'kode',width:120">Kode Mata Kuliah</th>
                <th data-options="field:'nama',width:350">Nama Mata Kuliah</th>
                <th data-options="field:'sks',width:80,align:'right'">SKS</th>
                <th data-options="field:'kelas',width:80,align:'right'">Kelas</th>
				<th data-options="field:'nil_huruf',width:80,align:'right'">Nilai</th>
            </tr>
        </thead>
    </table>
	<div id="toolbar">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" id="btn_hapus" onclick="hapusMatkul();">Hapus</a>
	</div>
	</div>
	<script type="text/javascript">
		function hapusMatkul() {

		}
		$(document).ready(function() {
			function cari() {
				var nrp = $("#search").val();
				if (nrp === "") {
					$.messager.alert('Peringatan','NRP belum dimasukkan!','warning');
				} else {
					$.ajax({
						url: 'app-frs.php?action=getnrp&nrp='+nrp,
						method: 'GET',
						success: function(result) {
							if (result.total === 0) {
								/*
								bootbox.alert({
									message : "NRP <strong>"+ nrp +"</strong> Tidak Ditemukan",
									callback : function() {
										$('#panel-mhs').dialog('close');
										$('#panel-matkul').dialog('closes');
									}
								});
								*/
								$.messager.alert({
									title: 'Peringatan',
									icon: 'error',
									msg: "NRP <strong>"+ nrp +"</strong> Tidak Ditemukan",
									fn : function() {
										$('#panel-mhs').dialog('close');
										$('#panel-matkul').dialog('close');
									}
								})
							} else {
								$('#panel-mhs').dialog('open');
								$('#panel-matkul').dialog('open');
								$("#dat_nama").html(result.rows[0]['nama']);
								$("#dat_nrp").html(result.rows[0]['nrp']);
								$("#dat_wali").html(result.rows[0]['nama_dosen']);
								$("#btn_ganti").click();
							}
						}
					});
				}
			}

			function load() {
				var periode = $("#periode").val();
				var tahun = $("#tahun").val();
				var ajaran = tahun.substring(2,4)+"0"+periode;
				var nrp = $("#dat_nrp").html();
				$.getJSON('app-frs.php?action=getfrs&nrp='+ nrp +'&periode='+ajaran, function(result){
					//$.each(result, function(i, field){
						//$("div").append(field + " ");
					//});
					//alert(result['total']);
					$('#table_frs').datagrid({data:result['rows']});
					$("#jml_sks").html(result['sks']);
					$("#jml_mk").html(result['total']);
				});
			}
			$("#btn_search").click(function(){
				//$("#_easyui_textbox_input1").val("17201");
				//$("#search").val("17201");
				cari();
				//alert("KLIK");
			});
			$("#btn_ganti").click(function() {
				var periode = $("#periode").val();
				var tahun = $("#tahun").val();
				var ajaran = tahun.substring(2,4)+"0"+periode;
				var nrp = $("#dat_nrp").html();
				$("#frm_nrp").val(nrp);
				$("#frm_periode").val(ajaran);
				$('#nama_matkul').combobox('clear');
				$('#nama_matkul').combobox('reload','app-frs.php?action=getmatkul&ajaran='+ajaran);
				//$('#table_frs').datagrid({'url': 'app-frs.php?action=getfrs&nrp='+ nrp +'&periode='+ajaran});
				load();

			});
			$("#btn_ambil").click(function(){
				var nrp = $("#frm_nrp").val();
				var periode = $("#frm_periode").val();
				$('#frm_matkul').form('submit',{
					url: 'app-frs.php?action=savefrs',
					onSubmit: function(){
						return $(this).form('validate');
					},
					success: function(result){
						var result = eval('('+result+')');
						if (result.errorMsg){
							$.messager.alert({
								title: 'Peringatan',
								msg: result.errorMsg,
								icon: 'error',
								fn: function() {
									load();
								}
							});
							/*
							bootbox.alert({
								message : 'Tidak dapat memasukkan Mata Kuliah yang sama',
								size : 'small'});*/
						} else {
							//$('#dlg').dialog('close');		// close the dialog
							//$('#table_frs').datagrid({'url': 'app-frs.php?action=getfrs&nrp='+ nrp +'&periode='+periode});	// reload the user data
							load();
						}
					}
				});
			});
			$("#btn_hapus").click(function(){
				var row = $('#table_frs').datagrid('getSelected');
				if (row){

					$.messager.confirm('Konfirmasi','Menghapus data mata kuliah '+row.kode+' '+row.nama+' ?',function(r){
						if (r){
							$.post('app-frs.php?action=hapusfrs',{id:row.id},function(result){
								if (result.success){
									//$('#dg').datagrid('reload');	// reload the user data
									load();
								} else {
									/*
									$.messager.show({	// show error message
										title: 'Error',
										msg: result.errorMsg
									});
									*/
									$.messager.alert('Error',result.errorMsgs,'error');
								}
							},'json');
						}
					});

					/*bootbox.confirm({
						title: 'Konfirmasi',
						message: 'Menghapus data mata kuliah<strong> '+row.kode+' '+row.nama+'</strong> ?',
						buttons: {
							cancel: {
								label: '<i class="fa fa-times"></i> Tidak'
							},
							confirm: {
								label: '<i class="fa fa-check"></i> Ya'
							}
						},
						callback: function (result) {
							//console.log('This was logged in the callback: ' + result);
							if (result) {
								$.post('app-frs.php?action=hapusfrs',{id:row.id},function(result){
								if (result.success){
									//$('#dg').datagrid('reload');	// reload the user data
									load();
								} else {
									bootbox.alert({
										message : result.errorMsg,
										size : 'small'
									});
								}
							},'json');
							}
						}
					});*/
				}
			});
		});

	</script>
</body>
</html>
