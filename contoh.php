<?php
include("class.simtool.php");
include("conn.php");

$sim = new LetterPairSimilarity;
$strCari = "eko";

$result = $conn->query("select nrp,nama from mahasiswa");
$all = array();
while($row = $result->fetch_assoc()) {
    //$simValue = $sim->compareStrings($strCari,$row['nama']);
    $arr = array('nrp' => $row['nrp'], 'nama' => $row['nama']/*, 'value' => $simValue*/);
    array_push($all,$arr);
}
/*
$nilai = 0;
$nama = "";
$nrp = "";
foreach($all as $xxx) {
    if ($xxx['value'] > $nilai)
    {
        $nilai = $xxx['value'];
        $nrp = $xxx['nrp'];
        $nama = $xxx['nama'];
    }
}
echo json_encode(array('nrp' => $nrp, 'nama' => $nama, 'value' => $nilai));
*/
$arrHuruf = array('A'=>1,'B'=>2,'C'=>3,'D'=>4,'E'=>5,'F'=>6,'G'=>7,'H'=>8,'I'=>9,'J'=>10,
                  'K'=>11,'L'=>12,'M'=>13,'N'=>14,'O'=>15,'P'=>16,'Q'=>17,'R'=>18,'S'=>19,
                  'T'=>20,'U'=>21,'V'=>22,'W'=>23,'X'=>24,'Y'=>25,'Z'=>26);

$beda = 26;
$nrp = "";

foreach($all as $r) {
    $idxCari = $arrHuruf[strtoupper(substr($strCari,0,1))];
    $idxHasil = $arrHuruf[strtoupper(substr($r['nama'],0,1))];
    
    if (abs($idxCari - $idxHasil) < $beda) {
        $beda = abs($idxCari - $idxHasil);
        $nrp = $r['nama'];
    }
    
}
echo $nrp;
$conn->close();
?>