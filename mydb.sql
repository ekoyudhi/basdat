-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for mydb
CREATE DATABASE IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mydb`;

-- Dumping structure for table mydb.ajaran
CREATE TABLE IF NOT EXISTS `ajaran` (
  `frs` varchar(4) NOT NULL,
  `semester` varchar(60) NOT NULL,
  `tahun` smallint(5) NOT NULL DEFAULT '0',
  `periode` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`frs`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table mydb.ajaran: 3 rows
DELETE FROM `ajaran`;
/*!40000 ALTER TABLE `ajaran` DISABLE KEYS */;
INSERT INTO `ajaran` (`frs`, `semester`, `tahun`, `periode`) VALUES
	('1601', 'Gasal 2016/2017', 2016, 1),
	('1602', 'Genap 2016/2017', 2016, 2),
	('1701', 'Gasal 2017/2018', 2017, 1);
/*!40000 ALTER TABLE `ajaran` ENABLE KEYS */;

-- Dumping structure for table mydb.dosen
CREATE TABLE IF NOT EXISTS `dosen` (
  `nip` varchar(10) NOT NULL,
  `nama` varchar(60) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table mydb.dosen: 5 rows
DELETE FROM `dosen`;
/*!40000 ALTER TABLE `dosen` DISABLE KEYS */;
INSERT INTO `dosen` (`nip`, `nama`) VALUES
	('1310000001', 'Dr. Adhi Dharma Wibawa, ST., MT.'),
	('1310000002', 'Dr. Wirawan, Ir., DEA'),
	('1310000003', 'Dr. Surya Sumpeno, ST., MT.'),
	('1310000004', 'Dr. Endroyono, Ir., DEA'),
	('1310000005', 'Dr. Istas Pratomo, ST., MT.');
/*!40000 ALTER TABLE `dosen` ENABLE KEYS */;

-- Dumping structure for table mydb.kelas
CREATE TABLE IF NOT EXISTS `kelas` (
  `kid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `frs` varchar(4) NOT NULL,
  `rid` varchar(6) NOT NULL,
  `jam` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `sks` int(11) DEFAULT NULL,
  `kelas` varchar(2) NOT NULL DEFAULT '-' COMMENT 'A,B,C',
  PRIMARY KEY (`kid`),
  UNIQUE KEY `frs_2` (`frs`,`rid`,`jam`,`kode`),
  KEY `frs` (`frs`),
  KEY `rid` (`rid`),
  KEY `kode` (`kode`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table mydb.kelas: 5 rows
DELETE FROM `kelas`;
/*!40000 ALTER TABLE `kelas` DISABLE KEYS */;
INSERT INTO `kelas` (`kid`, `frs`, `rid`, `jam`, `kode`, `nama`, `sks`, `kelas`) VALUES
	(1, '1701', 'R-01', 1, 'TE1001', 'Pengenalan Bidang Riset', 2, '-'),
	(2, '1701', 'R-02', 3, 'TE1006', 'Sistem Manajemen Basis Data Terdistribusi', 3, '-'),
	(3, '1701', 'R-03', 2, 'TE1005', 'Manajemen Strategi', 3, '-'),
	(4, '1701', 'L-01', 4, 'TE1004', 'Manajemen Jaringan', 2, '-'),
	(5, '1701', 'R-03', 5, 'TE1009', 'Sistem dan Jaringan TIK', 2, '-');
/*!40000 ALTER TABLE `kelas` ENABLE KEYS */;

-- Dumping structure for table mydb.kuliah
CREATE TABLE IF NOT EXISTS `kuliah` (
  `kode` varchar(10) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `sks` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table mydb.kuliah: 5 rows
DELETE FROM `kuliah`;
/*!40000 ALTER TABLE `kuliah` DISABLE KEYS */;
INSERT INTO `kuliah` (`kode`, `nama`, `sks`) VALUES
	('TE1001', 'Pengenalan Bidang Riset', 2),
	('TE1004', 'Manajemen Jaringan', 2),
	('TE1005', 'Manajemen Strategi', 3),
	('TE1006', 'Sistem Manajemen Basis Data Terdistribusi', 3),
	('TE1009', 'Sistem dan Jaringan TIK', 2);
/*!40000 ALTER TABLE `kuliah` ENABLE KEYS */;

-- Dumping structure for table mydb.mahasiswa
CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `nrp` char(5) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `tglahir` date NOT NULL,
  `gaji` double NOT NULL DEFAULT '0',
  `alamat` varchar(255) DEFAULT NULL,
  `nip_wali` varchar(10) NOT NULL,
  `r` bigint(20) NOT NULL DEFAULT '0',
  `s` varchar(20) DEFAULT NULL,
  `flag` tinyint(4) DEFAULT '0' COMMENT '0 unlocked, 1 locked, 2 retired',
  PRIMARY KEY (`nrp`),
  KEY `nip` (`nip_wali`),
  KEY `nama` (`nama`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table mydb.mahasiswa: 9 rows
DELETE FROM `mahasiswa`;
/*!40000 ALTER TABLE `mahasiswa` DISABLE KEYS */;
INSERT INTO `mahasiswa` (`nrp`, `nama`, `tglahir`, `gaji`, `alamat`, `nip_wali`, `r`, `s`, `flag`) VALUES
	('17207', 'Ernin Niswatul Ukhwah', '1984-12-22', 3000000, 'Sidoarjo', '1310000003', 0, NULL, 0),
	('17203', 'Nurhadiyanto', '1980-05-04', 3000000, 'Sidoarjo', '1310000001', 0, NULL, 0),
	('17210', 'Dessy Isyana Sunaryati', '1984-10-12', 3000000, 'Surabaya', '1310000003', 0, NULL, 0),
	('17202', 'Eryca Dwi Huzaini R', '1980-05-17', 3000000, 'Sidoarjo', '1310000002', 0, NULL, 0),
	('17209', 'Nur Yusuf Oktavia', '1980-10-24', 3000000, 'Lamongan', '1310000002', 0, NULL, 0),
	('17214', 'Hendra Setiawan', '1988-06-24', 3000000, 'Kediri', '1310000001', 0, NULL, 0),
	('17215', 'Suwander Husada', '1979-12-01', 3000000, 'Gresik', '1310000002', 0, NULL, 0),
	('17201', 'Eko Yudhi Prastowo', '1986-10-22', 3000000, 'Kudus', '1310000001', 0, NULL, 0),
	('17218', 'Mohammad Wahyudi Nafii', '1981-06-18', 3000000, 'Sidoarjo', '1310000001', 0, NULL, 0);
/*!40000 ALTER TABLE `mahasiswa` ENABLE KEYS */;

-- Dumping structure for table mydb.pengajaran
CREATE TABLE IF NOT EXISTS `pengajaran` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `nip` varchar(10) NOT NULL,
  `num` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `kid_2` (`kid`,`nip`),
  KEY `kid` (`kid`),
  KEY `nip` (`nip`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table mydb.pengajaran: 6 rows
DELETE FROM `pengajaran`;
/*!40000 ALTER TABLE `pengajaran` DISABLE KEYS */;
INSERT INTO `pengajaran` (`id`, `kid`, `nip`, `num`) VALUES
	(1, 1, '1230000001', 1),
	(2, 2, '1230000002', 1),
	(3, 3, '1230000003', 1),
	(4, 4, '1230000004', 1),
	(5, 5, '1230000005', 1),
	(6, 6, '1230000001', 1);
/*!40000 ALTER TABLE `pengajaran` ENABLE KEYS */;

-- Dumping structure for table mydb.perkuliahan
CREATE TABLE IF NOT EXISTS `perkuliahan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frs` varchar(4) NOT NULL,
  `nrp` varchar(14) NOT NULL,
  `kid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `nil_huruf` varchar(2) DEFAULT NULL,
  `nil_angka` int(11) DEFAULT NULL,
  `setuju` int(11) NOT NULL DEFAULT '0' COMMENT '0 belum setuju; 1 setuju',
  PRIMARY KEY (`id`),
  UNIQUE KEY `frs_2` (`frs`,`nrp`,`kid`),
  KEY `frs` (`frs`),
  KEY `nrp` (`nrp`),
  KEY `kid` (`kid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table mydb.perkuliahan: 3 rows
DELETE FROM `perkuliahan`;
/*!40000 ALTER TABLE `perkuliahan` DISABLE KEYS */;
INSERT INTO `perkuliahan` (`id`, `frs`, `nrp`, `kid`, `nil_huruf`, `nil_angka`, `setuju`) VALUES
	(1, '1701', '17201', 1, NULL, NULL, 0),
	(2, '1701', '17201', 2, NULL, NULL, 0),
	(3, '1701', '17201', 3, NULL, NULL, 0);
/*!40000 ALTER TABLE `perkuliahan` ENABLE KEYS */;

-- Dumping structure for table mydb.perwalian
CREATE TABLE IF NOT EXISTS `perwalian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frs` varchar(4) NOT NULL,
  `nrp` varchar(10) NOT NULL,
  `sks` smallint(5) unsigned NOT NULL DEFAULT '0',
  `setuju` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `frs_2` (`frs`,`nrp`),
  KEY `frs` (`frs`),
  KEY `nrp` (`nrp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table mydb.perwalian: 0 rows
DELETE FROM `perwalian`;
/*!40000 ALTER TABLE `perwalian` DISABLE KEYS */;
/*!40000 ALTER TABLE `perwalian` ENABLE KEYS */;

-- Dumping structure for table mydb.ruang
CREATE TABLE IF NOT EXISTS `ruang` (
  `rid` varchar(6) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `muat` smallint(5) NOT NULL DEFAULT '0',
  `lokasi` varchar(200) DEFAULT NULL,
  `catatan` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table mydb.ruang: 4 rows
DELETE FROM `ruang`;
/*!40000 ALTER TABLE `ruang` DISABLE KEYS */;
INSERT INTO `ruang` (`rid`, `nama`, `muat`, `lokasi`, `catatan`) VALUES
	('R-01', 'Ruang Kuliah 01', 14, NULL, NULL),
	('R-02', 'Ruang Kuliah 02', 12, NULL, NULL),
	('R-03', 'Ruang Kuliah 03', 11, NULL, NULL),
	('L-01', 'Ruang Lab 01', 10, NULL, NULL);
/*!40000 ALTER TABLE `ruang` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
