<?php
include 'conn.php';
include 'fungsi.php';

$action = $_GET['action'];

if ($action == 'get') {
    $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;
	$result = array();

	$rs = $conn->query("select count(*) from ruang");
	$row = $rs->fetch_row();
	$result["total"] = $row[0];
	$rs = $conn->query("select * from ruang limit $offset,$rows");
	$items = array();
	while($row = $rs->fetch_object()){
		array_push($items, $row);
	}
	$result["rows"] = $items;

	echo json_encode($result);
}
else if ($action == 'save') {
    $rid = htmlspecialchars($_REQUEST['rid']);
    $nama = htmlspecialchars($_REQUEST['nama']);	
	$muat = htmlspecialchars($_REQUEST['muat']);
	$lokasi = htmlspecialchars($_REQUEST['lokasi']);
	$catatan = htmlspecialchars($_REQUEST['catatan']);
    
    $sql = "insert into ruang(rid,nama,muat,lokasi,catatan) values('$rid','$nama','$muat','$lokasi','$catatan')";
    
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'rid' => $rid,
            'nama' => $nama,
			'muat' => $muat,
			'lokasi' => $lokasi,
			'catatan' => $catatan
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == 'update') {
    $rid = htmlspecialchars($_REQUEST['rid']);
    $nama = htmlspecialchars($_REQUEST['nama']);	
	$muat = htmlspecialchars($_REQUEST['muat']);
	$lokasi = htmlspecialchars($_REQUEST['lokasi']);
	$catatan = htmlspecialchars($_REQUEST['catatan']);
    
    $sql = "update ruang set nama='$nama', muat='$muat', lokasi='$lokasi', catatan='$catatan' where rid='$rid'";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'rid' => $rid,
            'nama' => $nama,
			'muat' => $muat,
			'lokasi' => $lokasi,
			'catatan' => $catatan
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == 'destroy') {
    $rid = htmlspecialchars($_REQUEST['rid']);

    $sql = "delete from ruang where rid='$rid'";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array('success'=>true));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}

$conn->close();
?>