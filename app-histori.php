<?php
include 'conn.php';
include 'fungsi.php';

$action = $_GET['action'];

    if ( $action == "getmatkul") {
        $ajaran = htmlspecialchars($_REQUEST['ajaran']);
        $rs = $conn->query("SELECT * FROM kelas WHERE frs='$ajaran'");
        $hasil = array();
        while($row = $rs->fetch_assoc()) {
            $r = array("id" => $row['kid'], "nama" => $row['kode']." | ".$row['nama']);
            array_push($hasil,$r);

        }
        header('Content-Type: application/json');
        echo json_encode($hasil);
    } else if ($action == "getnrp") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $rs = $conn->query("select mahasiswa.*, dosen.nama as nama_dosen from mahasiswa, dosen where mahasiswa.nip_wali = dosen.nip and mahasiswa.nrp='$nrp'");
        $total = $rs->num_rows;
        $hasil = array();
        while($row = $rs->fetch_assoc()) {
            array_push($hasil,$row);
        }
        header('Content-Type: application/json');
        echo json_encode(array("total" => $total,"rows"=>$hasil));
    }
    else if ($action == "getfrs") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);

        $sql = "select a.id,a.kid,a.frs, b.kode, b.nama, b.sks, b.kelas, a.nil_huruf, a.nil_angka
                from perkuliahan a left join kelas b on a.kid=b.kid
                Where nrp='$nrp' order by kode, frs";

        $rs = $conn->query($sql);
        $all = array();
        $nilai_mutu = 0;
        $sks = 0;
        $ipk = 0;
        while ($row = $rs->fetch_assoc()) {
          $nilai_mutu += (doubleval($row['nil_angka']) * doubleval($row['sks']));
          $sks += intval($row['sks']);
          array_push($all,$row);
        }
        Header('Content-Type: application/json');
        $result['total'] = count($all);
        $result['rows'] = $all;
        $result['sks'] = $sks;
        echo json_encode($result);
    }
else if ($action == "getmhs") {
  $sql = "SELECT nrp, nama from mahasiswa order by nrp";
  $rs = $conn->query($sql);
  $hasil = array();
  while ($row = $rs->fetch_assoc()) {
    array_push($hasil, $row);
  }
  echo json_encode($hasil);
}

$conn->close();
?>
