<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Formulir Rencana Studi</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/easyui.css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="stylesheet" type="text/css" href="css/color.css">
	<link rel="stylesheet" type="text/css" href="css/demo.css">
	<link rel="stylesheet" type="text/css" href="css/apps.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">

	<script src="jss/jquery.min.js"></script>
	<script src="jss/bootstrap.min.js"></script>
	<script src="jss/bootbox.min.js"></script>
	<script type="text/javascript" src="jss/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="jss/apps.js"></script>
	<script type="text/javascript" src="jss/jquery.dataTables.min.js"></script>
    <script type="text/javascript">

    </script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable( {
				"paging":   false,
				"ordering": false,
				"info":     false
			} );
		} );
	</script>
	<style type="text/css">
		td {
			padding: 0px 2px 0px 2px;
		}
	</style>
</head>
<body>
	<?php include "inc_nav.php"; ?>
	<h2>Nilai Mahasiswa</h2>
    <label>NRP : </label>
		<!--input id="search" name="search" style="width:200px" class="easyui-textbox" maxlength="5"-->
		<input id="search" type="hidden" value="">
		<select id="cc" class="easyui-combogrid" name="cc" style="width:250px;"
        data-options="
            panelWidth:450,
            value:'',
            idField:'nrp',
            textField:'nrp',
            url:'app-nilai.php?action=getmhs',
            columns:[[
                {field:'nrp',title:'NRP',width:60},
                {field:'nama',title:'Nama',width:390}
            ]],
						onChange: function(newValue,oldValue){
							$('#search').val(newValue);
						}
        "></select>
    <a href="javascript:void(0)" iconCls="icon-search" id="btn_search" class="easyui-linkbutton">Cari</a>
	<a href="" iconCls="icon-clear" id="btn_clear" class="easyui-linkbutton">Reset</a>
    <div style="height: 30px"></div>
    <div id="panel-mhs" closed="true" class="easyui-panel" title="Data Mahasiswa" style="width:100%;max-width: 880px;padding:20px;">
		<table>
			<tr>
				<td width="160">Nama</td><td width="10" align="center">:</td><td width="250"><div id="dat_nama"></div></td><td width="20"></td>
				<td width="160">Periode</td><td width="10" align="center">:</td><td width="250"><div id="dat_ajaran">
				<select id="periode">
					<option value="1">Gasal</option>
					<option value="2">Genap</option>
				</select>
				<select id="tahun">
					<option value="2017">2017/2018</option>
					<option value="2016">2016/2017</option>
				</select>
				<a href="javascript:void(0)" id="btn_ganti" class="easyui-linkbutton">Ganti</a>
				</div></td>
			</tr>
			<tr>
				<td width="160">NRP</td><td width="10" align="center">:</td><td width="250"><div id="dat_nrp"></div></td><td width="20"></td>
				<td width="160">Jumlah SKS Diambil</td><td width="10" align="center">:</td><td width="250"><div id="jml_sks">0</div></td>
			</tr>
			<tr>
				<td width="160">Dosen Wali</td><td width="10" align="center">:</td><td width="250"><div id="dat_wali"></div></td><td width="20"></td>
				<td width="160">Jumlah MK Diambil</td><td width="10" align="center">:</td><td width="250"><div id="jml_mk">0</div></td>
				<!--td width="160">Periode</td><td width="10" align="center">:</td><td width="250"></td-->
			</tr>
		</table>
	</div>
	<div style="height: 30px"></div>
	<div id="panel-matkul" closed="true" class="easyui-panel" style="border: 0px;">
	<!--form id="frm_matkul" method="post" novalidate>
	<input type="hidden" id="frm_nrp" name="frm_nrp">
	<input type="hidden" id="frm_periode" name="frm_periode">
	<label>Mata Kuliah : </label>
	<input id="nama_matkul" name="nama_matkul" class="easyui-combobox" style="width:100%;max-width:400px;" required="true"
				data-options="valueField:'id',textField:'nama'">
	    <a href="javascript:void(0)" iconCls="icon-ok" onclick="" id="btn_ambil" class="easyui-linkbutton">Ambil</a>
	</form-->
	<div style="height: 30px"></div>
	<table id="table_nilai" class="easyui-datagrid" style="width:100%;height:auto"
            toolbar="#toolbar" fitColumns="true" data-options="singleSelect:true,url:'',method:'get'">
        <thead>
            <tr>
				<th data-options="field:'id',width:40,hidden:true">ID</th>
                <th data-options="field:'kode',width:120">Kode Mata Kuliah</th>
                <th data-options="field:'nama',width:350">Nama Mata Kuliah</th>
                <th data-options="field:'sks',width:80,align:'right'">SKS</th>
                <th data-options="field:'kelas',width:80,align:'right'">Kelas</th>
				<th data-options="field:'nil_huruf',width:80,align:'right'">Nilai</th>
            </tr>
        </thead>
    </table>
	<div id="toolbar">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" id="btn_hapus" onclick="hapusMatkul();">Hapus</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="inputNilai()">Input Nilai</a>
	</div>
	</div>

	<div id="dlg" class="easyui-dialog" style="width:500px;height:auto;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<form id="fm" method="post" novalidate>
			<div class="fitem">
				<label>Nama MK :</label>
				<input id="nama" name="nama" class="easyui-textbox" required="true" disabled="true" style="width: 300px;">
				<input id="id" name="id" type="hidden">
			</div>
			<div class="fitem">
				<label>Nilai:</label>
                <!--input class="easyui-combobox" data-options="
                		valueField: 'label',
                		textField: 'nilai_huruf',
                		data: [{
                			label: 'A',
                			nilai_huruf: 'A'
                		},{
                			label: 'AB',
                			nilai_huruf: 'AB'
                		},{
                			label: 'B',
                			nilai_huruf: 'B'
                		},{
                			label: 'BC',
                			nilai_huruf: 'BC'
                		},{
                			label: 'C',
                			nilai_huruf: 'C'
                		},{
                			label: 'D',
                			nilai_huruf: 'D'
                		},{
                			label: 'E',
                			nilai_huruf: 'E'
                		}]" /-->

                <select id="nilai_huruf" class="easyui-combobox" name="nilai_huruf" style="width:100px;">
                    <option value="A">A</option>
                    <option value="AB">AB</option>
                    <option value="B">B</option>
                    <option value="BC">BC</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                </select>
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveNilai()" style="width:90px">Save</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
	</div>

	<script type="text/javascript">
        var nrp;
				var ajaran;
		function hapusMatkul() {

		}
		function inputNilai(){
			var row = $('#table_nilai').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','Input Nilai');
				$('#fm').form('load',row);
				$("#id").prop('disabled', true);
//                $('#nilai_huruf').combobox(select,'nilai_huruf');
				url = 'app-nilai.php?action=update&id='+row.id;
			}
		}

		function saveNilai(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlg').dialog('close');		// close the dialog
//						$('#table_nilai').datagrid('reload');	// reload the user data
						//$('#table_nilai').datagrid('reload','app-nilai.php?action=getfrs&nrp='+ nrp +'&periode='+ajaran);	// reload the user data
						$("#btn_ganti").click();

					}
				}
			});
		}

		$(document).ready(function() {
			function cari() {
				var nrp = $("#search").val();
				if (nrp === "") {
					$.messager.alert('Peringatan','NRP belum dimasukkan!','warning');
				} else {
					$.ajax({
						url: 'app-nilai.php?action=getnrp&nrp='+nrp,
						method: 'GET',
						success: function(result) {
							if (result.total === 0) {
								$.messager.alert({
									title: 'Peringatan',
									icon: 'error',
									msg: "NRP <strong>"+ nrp +"</strong> Tidak Ditemukan",
									fn : function() {
										$('#panel-mhs').dialog('close');
										$('#panel-matkul').dialog('close');
									}
								})
							} else {
								$('#panel-mhs').dialog('open');
								$('#panel-matkul').dialog('open');
								$("#dat_nama").html(result.rows[0]['nama']);
								$("#dat_nrp").html(result.rows[0]['nrp']);
								$("#dat_wali").html(result.rows[0]['nama_dosen']);
								$("#btn_ganti").click();
							}
						}
					});
				}
			}

			function load() {
				var periode = $("#periode").val();
				var tahun = $("#tahun").val();
				ajaran = tahun.substring(2,4)+"0"+periode;
				nrp = $("#dat_nrp").html();
				$.getJSON('app-nilai.php?action=getfrs&nrp='+ nrp +'&periode='+ajaran, function(result){
					$('#table_nilai').datagrid({data:result['rows']});
					$("#jml_sks").html(result['sks']);
					$("#jml_mk").html(result['total']);
				});
			}



			$("#btn_search").click(function(){
				//$("#_easyui_textbox_input1").val("17201");
				//$("#search").val("17201");
				cari();
				//alert("KLIK");
			});
			$("#btn_ganti").click(function() {
				var periode = $("#periode").val();
				var tahun = $("#tahun").val();
				ajaran = tahun.substring(2,4)+"0"+periode;
				nrp = $("#dat_nrp").html();
				$("#frm_nrp").val(nrp);
				$("#frm_periode").val(ajaran);
				$('#nama_matkul').combobox('clear');
				$('#nama_matkul').combobox('reload','app-nilai.php?action=getmatkul&ajaran='+ajaran);
				//$('#table_nilai').datagrid({'url': 'app-nilai.php?action=getfrs&nrp='+ nrp +'&periode='+ajaran});
				load();

			});
			$("#btn_ambil").click(function(){
				nrp = $("#frm_nrp").val();
				var periode = $("#frm_periode").val();
				$('#frm_matkul').form('submit',{
					url: 'app-nilai.php?action=savefrs',
					onSubmit: function(){
						return $(this).form('validate');
					},
					success: function(result){
						var result = eval('('+result+')');
						if (result.errorMsg){
							$.messager.alert({
								title: 'Peringatan',
								msg: result.errorMsg,
								icon: 'error',
								fn: function() {
									load();
								}
							});
							/*
							bootbox.alert({
								message : 'Tidak dapat memasukkan Mata Kuliah yang sama',
								size : 'small'});*/
						} else {
							//$('#dlg').dialog('close');		// close the dialog
							//$('#table_nilai').datagrid({'url': 'app-nilai.php?action=getfrs&nrp='+ nrp +'&periode='+periode});	// reload the user data
							load();
						}
					}
				});
			});
			$("#btn_hapus").click(function(){
				var row = $('#table_nilai').datagrid('getSelected');
				if (row){

					$.messager.confirm('Konfirmasi','Menghapus data mata kuliah '+row.kode+' '+row.nama+' ?',function(r){
						if (r){
							$.post('app-nilai.php?action=hapusfrs',{id:row.id},function(result){
								if (result.success){
									//$('#dg').datagrid('reload');	// reload the user data
									load();
								} else {
									/*
									$.messager.show({	// show error message
										title: 'Error',
										msg: result.errorMsg
									});
									*/
									$.messager.alert('Error',result.errorMsgs,'error');
								}
							},'json');
						}
					});

					/*bootbox.confirm({
						title: 'Konfirmasi',
						message: 'Menghapus data mata kuliah<strong> '+row.kode+' '+row.nama+'</strong> ?',
						buttons: {
							cancel: {
								label: '<i class="fa fa-times"></i> Tidak'
							},
							confirm: {
								label: '<i class="fa fa-check"></i> Ya'
							}
						},
						callback: function (result) {
							//console.log('This was logged in the callback: ' + result);
							if (result) {
								$.post('app-nilai.php?action=hapusfrs',{id:row.id},function(result){
								if (result.success){
									//$('#dg').datagrid('reload');	// reload the user data
									load();
								} else {
									bootbox.alert({
										message : result.errorMsg,
										size : 'small'
									});
								}
							},'json');
							}
						}
					});*/
				}
			});


		});


	</script>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
</body>
</html>
