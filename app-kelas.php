<?php
include 'conn.php';
include 'fungsi.php';

$action = $_GET['action'];

if ($action == 'get') {
    $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;
	$result = array();

	$rs = $conn->query("select count(*) from kelas");
	$row = $rs->fetch_row();
	$result["total"] = $row[0];
	$rs = $conn->query("select * from kelas order by frs limit $offset,$rows");
	$rs_a = $conn->query("select * from ajaran");
	$rs_r = $conn->query("select * from ruang");
	$items = array();
	while($row = $rs->fetch_assoc()){
		foreach($rs_a as $r1) {
			if ($row['frs'] == $r1['frs']) {
				$row['semester'] = $r1['semester'];
			}
		}
		foreach($rs_r as $r2) {
			if ($row['rid'] == $r2['rid']) {
				$row['rnama'] = $r2['nama'];
			}
		}
		array_push($items, $row);
	}
	$result["rows"] = $items;

	echo json_encode($result);
}
else if ($action == 'save') {
  $semester = htmlspecialchars($_REQUEST['semester']);
  $rnama = htmlspecialchars($_REQUEST['rnama']);
	$nama = htmlspecialchars($_REQUEST['nama']);
	$jam = htmlspecialchars($_REQUEST['jam']);
	$kelas = htmlspecialchars($_REQUEST['kelas']);
    if ($kelas == "") {
		$kelas = "-";
	}
	//$rs = $conn->query("SELECT kode,nama,sks FROM kuliah where kode='$nama'");
	//$matkul = $rs->fetch_assoc();
  $mat = explode("|",$nama);
	$kode = trim($mat[0]);
	$nm = trim($mat[2]);
	$sks = trim($mat[1]);
    $sql = "insert into kelas(frs,rid,jam,kelas,kode,nama,sks) values('$semester','$rnama','$jam','$kelas','$kode','$nm','$sks')";

    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'frs' => $semester,
            'rid' => $rnama,
			'jam' => $jam,
			'kelas' => $kelas,
			'kode' => $kode,
			'nama' => $nm,
			'sks' => $sks
        ));
    } else {
        echo json_encode(array('errorMsg'=> $conn->error));
    }
}
else if ($action == 'update') {
    $nip = htmlspecialchars($_REQUEST['nip']);
    $nama = htmlspecialchars($_REQUEST['nama']);

    $sql = "update dosen set nama='$nama' where nip=$nip";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'nip' => $nip,
            'nama' => $nama
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == 'destroy') {
    $kid = htmlspecialchars($_REQUEST['kid']);

	$rescek = $conn->query("select count(*) from perkuliahan where kid='$kid'");
	$jmlcek = $rescek->fetch_row();
	if ($jmlcek[0] > 0) {
		echo json_encode(array('errorMsg'=>'Data kelas tidak dapat dihapus karena dipakai pada tabel FRS'));
	}
	else {
		$sql = "delete from kelas where kid='$kid'";

		$result = $conn->query($sql);
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('errorMsg'=> $conn->error));
		}
	}
}
else if ($action == "getcombo") {
	$item = htmlspecialchars($_REQUEST['item']);

	$hasil = array();
	if ($item == "semester") {
		$rs = $conn->query("SELECT frs, semester from ajaran order by tahun desc, periode desc");
		while ($row = $rs->fetch_assoc()) {
			array_push($hasil, $row);
		}
	}
	else if ($item == "ruang") {
		$rs = $conn->query("SELECT rid, nama as rnama from ruang");
		while ($row = $rs->fetch_assoc()) {
			array_push($hasil, $row);
		}
	}
	else if ($item == "matkul") {
    $periode = htmlspecialchars($_REQUEST['periode']);
    $pr = intval(substr($periode,0,2));
    $tahun_kur = 2000+$pr;
    $arr = array();
    $rs_th = $conn->query("SELECT DISTINCT tahun from kurikulum");
    while ($row = $rs_th->fetch_assoc()) {
      if (intval($row['tahun']) <= $tahun_kur)
      array_push($arr, $row['tahun']);
    }
    $th = max($arr);
    $rs = $conn->query("SELECT kode, nama, sks FROM kurikulum where tahun='$th'");
		//$rs = $conn->query("SELECT kode, nama from kuliah");
		while ($row = $rs->fetch_assoc()) {
			//array_push($hasil, $row);
      array_push($hasil, array('kode'=>$row['kode'], 'nama' => $row['kode'].' | '.$row['sks'].' | '.$row['nama']));
		}
	}
	header('Content-Type: application/json');
	echo json_encode($hasil);
}

$conn->close();
?>
