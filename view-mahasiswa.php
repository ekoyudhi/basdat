<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Data Mahasiswa</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/easyui.css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="stylesheet" type="text/css" href="css/color.css">
	<link rel="stylesheet" type="text/css" href="css/demo.css">
	<link rel="stylesheet" type="text/css" href="css/apps.css">
	
	<script src="jss/jquery.min.js"></script>
	<script src="jss/bootstrap.min.js"></script>
	<script type="text/javascript" src="jss/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="jss/apps.js"></script>
	<script type="text/javascript" src="jss/bootbox.min.js"></script>
	
</head>
<body>
	<?php include "inc_nav.php"; ?>
	<h2>Data Mahasiswa</h2>
	<!--p>Click the buttons on datagrid toolbar to do crud actions.</p-->
	
	<table id="dg" title="Mahasiswa" class="easyui-datagrid" style="width:100%;height:auto"
			url = "app-mahasiswa.php?action=get"
			toolbar="#toolbar" pagination="true"
			rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
				<th field="nrp" width="60">NRP</th>
				<th field="nama" width="100">Nama</th>
				<th field="alamat" width="150">Alamat</th>
				<th field="tglahir" width="40">Tanggal Lahir</th>
				<th field="gaji" width="50">Gaji</th>
				<th field="nama_dosen" width="100">Nama Dosen Wali</th>
			</tr>
		</thead>
	</table>
	<div id="toolbar">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="reloadUser()">Reload</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">Rekam</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Ubah</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Hapus</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="cariUser()">Cari</a>
	</div>
	
	<div id="dialog_cari" class="easyui-dialog" style="width: auto;height: auto;padding: 10px 20px"
		 closed="true" buttons="#dialog_cari-buttons">
		<div class="ftitle">Pencarian</div>
		<form id="frm_cari" method="post" novalidate=>
			<div class="fitem">
				<label>NRP :</label>
				<input id="cari_nrp" name="cari_nrp" class="easyui-textbox" maxlength=5 >
			</div>
			<div class="fitem">
				<label>Nama :</label>
				<input id="cari_nama" name="cari_nama" class="easyui-textbox">
			</div>
		</form>
	</div>
	<div id="dialog_cari-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-search" onclick="cariAction()" style="width:90px">Cari</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dialog_cari').dialog('close')" style="width:90px">Cancel</a>
	</div>
	<div id="dlg" class="easyui-dialog" style="width:auto;height:auto;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<div class="ftitle">Data Mahasiswa</div>
		<form id="fm" method="post" novalidate>
			<div class="fitem">
				<label>NRP:</label>
				<input id="nrp" name="nrp" class="easyui-validatebox" required="true" maxlength="5">
			</div>
			<div class="fitem">
				<label>Nama:</label>
				<input name="nama" class="easyui-textbox" required="true">
			</div>
			<div class="fitem">
				<label>Alamat:</label>
				<input name="alamat" class="easyui-textbox" required="true">
			</div>
			<div class="fitem">
				<label>Tanggal Lahir:</label>
				<input id="tglahir" name="tglahir" class="easyui-datebox" required="true" data-options="formatter:myformatter,parser:myparser">
			</div>
				<div class="fitem">
				<label>Gaji:</label>
				<input name="gaji" class="easyui-numberbox" value="0" data-options="min:0,precision:2" required="true">
			</div>
			<div class="fitem">
				<label>Wali:</label>
				<input id="nama_dosen" name="nama_dosen" class="easyui-combobox" required="true"
				data-options="valueField:'nip',textField:'nama',url:'app-mahasiswa.php?action=getdosen'">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
		<!--a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a-->
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="closeDialog();" style="width:90px">Cancel</a>
	</div>
	<script type="text/javascript">
		var url;
		var nrp;
		function selectUser() {
			//$('#dg').datagrid('selectRow',1);
			alert("Changed!");
		}
		function coba() {
			$(document).ready(function() {
			$.ajax({
						url: 'app-mahasiswa.php?action=getnrp&nrp=17201',
						method: 'GET'
					})
					.success(function(response){
						alert(response);
					});
			});
		}
		function closeDialog() {
			$('#dlg').dialog('close');
			//$.get("app-mahasiswa.php?action=unlock&nrp="+nrp);

		}
		function reloadUser() {
			$('#dg').datagrid({
				'url' : 'app-mahasiswa.php?action=get',
				rowStyler: function(index,row) {
					return '';
				}
			});
		}

		function cariUser() {
			$('#dialog_cari').dialog('open').dialog('setTitle','Pencarian');
			$('#frm_cari').form('clear');
			url = 'app-mahasiswa.php?action=cari';
		}
		function cariAction() {
			$('#frm_cari').form('submit',{
				url : url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						//var nrp = $('#cari_nrp').val();
						//var nama = $('#cari_nama').val();
						//var idx = 0;
						$('#dialog_cari').dialog('close');		// close the dialog
						//alert(result.rows[0]['nama']);
						$('#dg').datagrid({
							//'url' : 'app-mahasiswa.php?action=cari&nrp='+nrp+'&nama='+nama
							rowStyler: function(index,row) {
								for(var i=0;i<result.rows.length;i++) {
									if (row.nrp==result.rows[i]['nrp']){
										
										return 'background-color:#6293BB;color:#fff;'; // return inline style
										// the function can return predefined css class and inline style
										// return {class:'r1', style:{'color:#fff'}};	
									}
								}
							}
						});	// reload the user data
						
					}
				}
			});
		}
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Input Mahasiswa Baru');
			$('#fm').form('clear');
			$('#nrp').prop('disabled',false);
			url = 'app-mahasiswa.php?action=save';
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			nrp = row.nrp;
			//
			$.getJSON('app-mahasiswa.php?action=getlock&nrp='+row.nrp, function(result){
				if(result['condition'] < 1) {
					$.get("app-mahasiswa.php?action=lock&nrp="+row.nrp);
					if (row){
						$('#dlg').dialog('open').dialog('setTitle','Edit Mahasiswa');
						$('#fm').form('load',row);
						$('#nrp').prop('disabled',true);
						url = 'app-mahasiswa.php?action=update&nrp='+row.nrp;
					}
				} else {
					$.messager.alert('Peringatan','Data mahasiswa NRP : '+row.nrp+' Nama : '+row.nama+' sedang diubah oleh user lain','error');
				}
			});
			$('#dlg').dialog({
				onClose: function() {
					//alert(nrp);
					$.get("app-mahasiswa.php?action=unlock&nrp="+nrp);
				}
			});
			
		}
		function saveUser(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlg').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Konfirmasi','Apakah anda akan menghapus data mahasiswa NRP : '+row.nrp+' Nama : '+row.nama+' ?',function(r){
					if (r){
						$.post('app-mahasiswa.php?action=destroy',{nrp:row.nrp},function(result){
							if (result.success){
								$('#dg').datagrid('reload');	// reload the user data
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							}
						},'json');
					}
				});
			}
		}
		
		//parser
		function myformatter(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            //return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
			return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
        }
        function myparser(s){
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[2],10);
            var m = parseInt(ss[1],10);
            var d = parseInt(ss[0],10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                return new Date(y,m-1,d);
            } else {
                return new Date();
            }
        }
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#nrp").keyup(function() {
				var dat = $("#nrp").val();
				if (dat.length == 5) {
					$.get("app-mahasiswa.php?action=getnrp&nrp="+dat, function(data){
						if (data.jumlah == 1) {
							/*
							bootbox.alert({
								message: "NRP sudah ada. Masukkan NRP yang berbeda!",
								callback: function () {
									//console.log('This was logged in the callback!');
									$("#nrp").val("");
									$("#nrp").focus();
								}
							});*/
							$.messager.alert({	// show error message
									title: 'Peringatan',
									msg: 'NRP sudah ada. Masukkan NRP yang berbeda!',
									icon: 'error',
									fn: function() {
										$("#nrp").val("");
										$("#nrp").focus();
									}
								});
						}
					});
					
				}
			});
		});
	</script>
	<script>
        $(function() {
          function grid_refresh() {
        	$('#dg').datagrid('reload'); // reload grid
            $('#dg').datagrid({loadMsg:''});        
        	setTimeout(grid_refresh, 3000); // schedule next refresh after 3sec
          }
        
          grid_refresh();
        });    
    </script>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
</body>
</html>