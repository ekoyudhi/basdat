-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 17, 2017 at 08:06 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--
CREATE DATABASE IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mydb`;

-- --------------------------------------------------------

--
-- Table structure for table `ajaran`
--

CREATE TABLE `ajaran` (
  `frs` varchar(4) NOT NULL,
  `semester` varchar(60) NOT NULL,
  `tahun` smallint(5) NOT NULL DEFAULT '0',
  `periode` tinyint(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ajaran`
--

INSERT INTO `ajaran` (`frs`, `semester`, `tahun`, `periode`) VALUES
('1601', 'Gasal 2016/2017', 2016, 1),
('1602', 'Genap 2016/2017', 2016, 2),
('1701', 'Gasal 2017/2018', 2017, 1),
('1501', 'Gasal 2015/2016', 2015, 1),
('1401', 'Gasal 2014/2015', 2014, 1),
('1301', 'Gasal 2013/2014', 2013, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `all_frs`
-- (See below for the actual view)
--
CREATE TABLE `all_frs` (
`id` int(10) unsigned
,`frs` varchar(4)
,`nrp` varchar(14)
,`kid` smallint(5) unsigned
,`nil_huruf` varchar(2)
,`nil_angka` double(11,2)
,`setuju` int(11)
,`kode` varchar(10)
,`nama` varchar(255)
,`sks` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `nip` varchar(10) NOT NULL,
  `nama` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`nip`, `nama`) VALUES
('1310000001', 'Dr. Adhi Dharma Wibawa, ST., MT.'),
('1310000002', 'Dr. Wirawan, Ir., DEA'),
('1310000003', 'Dr. Surya Sumpeno, ST., MT.'),
('1310000004', 'Dr. Endroyono, Ir., DEA'),
('1310000005', 'Dr. Istas Pratomo, ST., MT.');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kid` smallint(5) UNSIGNED NOT NULL,
  `frs` varchar(4) NOT NULL,
  `rid` varchar(6) NOT NULL,
  `jam` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `sks` int(11) DEFAULT NULL,
  `kelas` varchar(2) NOT NULL DEFAULT '-' COMMENT 'A,B,C'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kid`, `frs`, `rid`, `jam`, `kode`, `nama`, `sks`, `kelas`) VALUES
(8, '1601', 'R-01', 1, 'TE1001', 'Pengenalan Bidang Riset', 3, '-'),
(9, '1601', 'R-01', 1, 'TE1002', 'Basis Data', 3, '-'),
(10, '1601', 'R-02', 1, 'TE1004', 'Manajemen Strategi', 2, '-'),
(11, '1602', 'R-03', 1, 'TE1003', 'Kemanan Jaringan', 2, '-'),
(12, '1602', 'L-01', 1, 'TE1005', 'Penulisan Ilmiah', 2, '-'),
(13, '1602', 'R-02', 2, 'TE1006', 'Manajemen Proyek', 2, '-'),
(14, '1701', 'R-01', 2, 'TE1001', 'Riset', 2, '-'),
(15, '1701', 'R-01', 4, 'TE1002', 'Basis Data 2', 2, '-');

-- --------------------------------------------------------

--
-- Table structure for table `kuliah`
--

CREATE TABLE `kuliah` (
  `kode` varchar(10) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `sks` smallint(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kuliah`
--

INSERT INTO `kuliah` (`kode`, `nama`, `sks`) VALUES
('TE1001', 'Pengenalan Bidang Riset', 2),
('TE1004', 'Manajemen Jaringan 2', 2),
('TE1005', 'Manajemen Strategi', 3),
('TE1006', 'Sistem Manajemen Basis Data Terdistribusi', 3),
('TE1009', 'Sistem dan Jaringan TIK', 2);

-- --------------------------------------------------------

--
-- Table structure for table `kurikulum`
--

CREATE TABLE `kurikulum` (
  `id` int(11) NOT NULL,
  `tahun` smallint(6) NOT NULL,
  `semester` tinyint(4) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `sks` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurikulum`
--

INSERT INTO `kurikulum` (`id`, `tahun`, `semester`, `kode`, `nama`, `sks`) VALUES
(1, 2014, 1, 'TE1001', 'Pengenalan Bidang Riset', 3),
(5, 2014, 1, 'TE1002', 'Basis Data', 3),
(6, 2014, 2, 'TE1003', 'Kemanan Jaringan', 2),
(7, 2014, 1, 'TE1004', 'Manajemen Strategi', 2),
(10, 2014, 2, 'TE1005', 'Penulisan Ilmiah', 2),
(11, 2014, 2, 'TE1006', 'Manajemen Proyek', 2),
(12, 2014, 3, 'TE1007', 'E-Government', 2),
(13, 2014, 3, 'TE1008', 'Sistem Cerdas', 2),
(14, 2014, 3, 'TE1009', 'Biometrika', 2),
(15, 2014, 3, 'TE1010', 'Rekayasa Internet', 2),
(16, 2014, 4, 'TE1011', 'Audit TIK', 2),
(17, 2014, 4, 'TE1999', 'Tesis', 6),
(20, 2017, 1, 'TE1001', 'Riset', 2),
(21, 2017, 1, 'TE1002', 'Basis Data 2', 2);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nrp` char(5) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `tglahir` date NOT NULL,
  `gaji` double NOT NULL DEFAULT '0',
  `alamat` varchar(255) DEFAULT NULL,
  `nip_wali` varchar(10) NOT NULL,
  `r` bigint(20) NOT NULL DEFAULT '0',
  `s` varchar(20) DEFAULT NULL,
  `flag` tinyint(4) DEFAULT '0' COMMENT '0 unlocked, 1 locked, 2 retired'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nrp`, `nama`, `tglahir`, `gaji`, `alamat`, `nip_wali`, `r`, `s`, `flag`) VALUES
('17207', 'Ernin Niswatul Ukhwah', '1984-12-22', 3000000, 'Sidoarjo', '1310000003', 0, NULL, 0),
('17203', 'Nurhadiyanto', '1980-05-04', 3000000, 'Sidoarjo', '1310000001', 0, NULL, 0),
('17210', 'Dessy Isyana Sunaryati', '1984-10-12', 3000000, 'Surabaya', '1310000003', 0, NULL, 0),
('17202', 'Eryca Dwi Huzaini R', '1980-05-17', 3000000, 'Sidoarjo', '1310000002', 0, NULL, 0),
('17209', 'Nur Yusuf Oktavia', '1980-10-24', 3000000, 'Lamongan', '1310000002', 0, NULL, 0),
('17214', 'Hendra Setiawan', '1988-06-24', 3000000, 'Kediri', '1310000001', 0, NULL, 0),
('17215', 'Suwander Husada', '1979-12-01', 3000000, 'Gresik', '1310000002', 0, NULL, 0),
('17201', 'Eko Yudhi Prastowo', '1986-10-22', 3000000, 'Kudus', '1310000001', 0, NULL, 0),
('17218', 'Mohammad Wahyudi Nafii', '1981-06-18', 3000000, 'Sidoarjo', '1310000001', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengajaran`
--

CREATE TABLE `pengajaran` (
  `id` int(10) UNSIGNED NOT NULL,
  `kid` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `nip` varchar(10) NOT NULL,
  `num` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengajaran`
--

INSERT INTO `pengajaran` (`id`, `kid`, `nip`, `num`) VALUES
(1, 1, '1230000001', 1),
(2, 2, '1230000002', 1),
(3, 3, '1230000003', 1),
(4, 4, '1230000004', 1),
(5, 5, '1230000005', 1),
(6, 6, '1230000001', 1);

-- --------------------------------------------------------

--
-- Table structure for table `perkuliahan`
--

CREATE TABLE `perkuliahan` (
  `id` int(10) UNSIGNED NOT NULL,
  `frs` varchar(4) NOT NULL,
  `nrp` varchar(14) NOT NULL,
  `kid` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `nil_huruf` varchar(2) DEFAULT NULL,
  `nil_angka` double(11,2) DEFAULT NULL,
  `setuju` int(11) NOT NULL DEFAULT '0' COMMENT '0 belum setuju; 1 setuju'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perkuliahan`
--

INSERT INTO `perkuliahan` (`id`, `frs`, `nrp`, `kid`, `nil_huruf`, `nil_angka`, `setuju`) VALUES
(9, '1701', '17201', 14, 'AB', 3.50, 0),
(10, '1701', '17201', 15, 'A', 4.00, 0),
(7, '1601', '17201', 9, 'AB', 3.50, 0),
(8, '1601', '17201', 10, 'A', 4.00, 0),
(6, '1601', '17201', 8, 'A', 4.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `perwalian`
--

CREATE TABLE `perwalian` (
  `id` int(10) UNSIGNED NOT NULL,
  `frs` varchar(4) NOT NULL,
  `nrp` varchar(10) NOT NULL,
  `sks` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `setuju` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `rid` varchar(6) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `muat` smallint(5) NOT NULL DEFAULT '0',
  `lokasi` varchar(200) DEFAULT NULL,
  `catatan` varchar(600) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`rid`, `nama`, `muat`, `lokasi`, `catatan`) VALUES
('R-01', 'Ruang Kuliah 01', 14, NULL, NULL),
('R-02', 'Ruang Kuliah 02', 12, NULL, NULL),
('R-03', 'Ruang Kuliah 03', 11, NULL, NULL),
('L-01', 'Ruang Lab 01', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure for view `all_frs`
--
DROP TABLE IF EXISTS `all_frs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `all_frs`  AS  select `a`.`id` AS `id`,`a`.`frs` AS `frs`,`a`.`nrp` AS `nrp`,`a`.`kid` AS `kid`,`a`.`nil_huruf` AS `nil_huruf`,`a`.`nil_angka` AS `nil_angka`,`a`.`setuju` AS `setuju`,`b`.`kode` AS `kode`,`b`.`nama` AS `nama`,`b`.`sks` AS `sks` from (`perkuliahan` `a` join `kelas` `b` on((`a`.`kid` = `b`.`kid`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ajaran`
--
ALTER TABLE `ajaran`
  ADD PRIMARY KEY (`frs`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kid`),
  ADD UNIQUE KEY `frs_2` (`frs`,`rid`,`jam`,`kode`),
  ADD KEY `frs` (`frs`),
  ADD KEY `rid` (`rid`),
  ADD KEY `kode` (`kode`);

--
-- Indexes for table `kuliah`
--
ALTER TABLE `kuliah`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `kurikulum`
--
ALTER TABLE `kurikulum`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tahun_kode` (`tahun`,`kode`) USING BTREE,
  ADD KEY `kode` (`kode`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nrp`),
  ADD KEY `nip` (`nip_wali`),
  ADD KEY `nama` (`nama`);

--
-- Indexes for table `pengajaran`
--
ALTER TABLE `pengajaran`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kid_2` (`kid`,`nip`),
  ADD KEY `kid` (`kid`),
  ADD KEY `nip` (`nip`);

--
-- Indexes for table `perkuliahan`
--
ALTER TABLE `perkuliahan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frs_2` (`frs`,`nrp`,`kid`),
  ADD KEY `frs` (`frs`),
  ADD KEY `nrp` (`nrp`),
  ADD KEY `kid` (`kid`);

--
-- Indexes for table `perwalian`
--
ALTER TABLE `perwalian`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `frs_2` (`frs`,`nrp`),
  ADD KEY `frs` (`frs`),
  ADD KEY `nrp` (`nrp`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`rid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `kid` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `kurikulum`
--
ALTER TABLE `kurikulum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pengajaran`
--
ALTER TABLE `pengajaran`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `perkuliahan`
--
ALTER TABLE `perkuliahan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `perwalian`
--
ALTER TABLE `perwalian`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
