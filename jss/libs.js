(function($) {

	$_script = function(id, html, _event) {
		if (!html) return;
		var script = document.getElementsByTagName('script')[0];
		var _el = document.createElement('script');
		_el.id = id.replace('#', '');
		_el.type = (typeof _event == 'string') ? _event : 'text/html';
		_el.text = html.trim();
		script.parentNode.insertBefore(_el, script);
		if (typeof _event == 'function') _event(_el);
	};

	$.script = function(id, url, _event) {
		$.get(url, function(html) { $_script(id, html, _event); });
	};

	$.link = function(url, media) {
		var link = document.getElementsByTagName('link')[0];
		var _el = document.createElement('link');
		_el.rel = 'stylesheet';
		_el.type = 'text/css';
		_el.href = url;
		_el.media = media ? media : 'all';
		link.parentNode.insertBefore(_el, link);
	};

})(jQuery);

