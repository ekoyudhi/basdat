<?php
include 'conn.php';
include 'fungsi.php';

$action = $_GET['action'];


/*
if ($action == 'get') {
    $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;
	$result = array();

	$rs = $conn->query("select count(*) from perkuliahan");
	$row = $rs->fetch_row();
	$result["total"] = $row[0];
	$rs = $conn->query("select * from perkuliahan limit $offset,$rows");
	$items = array();
	while($row = $rs->fetch_object()){
		array_push($items, $row);
	}
	$result["rows"] = $items;

	echo json_encode($result);
}
else if ($action == 'save') {
    $nip = htmlspecialchars($_REQUEST['nip']);
    $nama = htmlspecialchars($_REQUEST['nama']);


    $sql = "insert into dosen(nip,nama) values('$nip','$nama')";

    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'nip' => $nip,
            'nama' => $nama
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
*/

    if ( $action == "getmatkul") {
        $ajaran = htmlspecialchars($_REQUEST['ajaran']);
        $rs = $conn->query("SELECT * FROM kelas WHERE frs='$ajaran'");
        $hasil = array();
        while($row = $rs->fetch_assoc()) {
            $r = array("id" => $row['kid'], "nama" => $row['kode']." | ".$row['nama']);
            array_push($hasil,$r);

        }
        header('Content-Type: application/json');
        echo json_encode($hasil);
    } else if ($action == "getnrp") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $rs = $conn->query("select mahasiswa.*, dosen.nama as nama_dosen from mahasiswa, dosen where mahasiswa.nip_wali = dosen.nip and mahasiswa.nrp='$nrp'");
        $total = $rs->num_rows;
        $hasil = array();
        while($row = $rs->fetch_assoc()) {
            array_push($hasil,$row);
        }
        header('Content-Type: application/json');
        echo json_encode(array("total" => $total,"rows"=>$hasil));
    }
    else if ($action == "getAllAjaran") {

    }
    else if ($action == "getfrs") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $periode = htmlspecialchars($_REQUEST['periode']);
        //kid, kode matkul
        $sql = "select a.id,a.kid, b.kode, b.nama, b.sks, b.kelas, a.nil_huruf from (select * from perkuliahan where nrp='$nrp' and frs='$periode') as a left join (select * from kelas) as b on a.kid=b.kid";
        $rs = $conn->query($sql);
        $result['total'] = $rs->num_rows;
        $items = array();
        $sks = 0;
        while($row = $rs->fetch_assoc()){
            array_push($items, $row);
            $sks += $row['sks'];
        }
        $result["rows"] = $items;
        $result["sks"] = $sks;

        echo json_encode($result);
    }
    else if ($action == "savefrs") {
        $nrp = htmlspecialchars($_REQUEST['frm_nrp']);
        $periode = htmlspecialchars($_REQUEST['frm_periode']);
        $kid = htmlspecialchars($_REQUEST['nama_matkul']);

        $sql = "INSERT INTO perkuliahan(frs,nrp,kid) VALUES ('$periode','$nrp','$kid')";
        $result = $conn->query($sql);
        if ($result){
            echo json_encode(array(
                'nrp' => $nrp,
                'periode' => $periode,
                'kid' => $kid
            ));
        } else {
            echo json_encode(array('errorMsg'=> $conn->error));
        }
    }
else if ($action == 'update') {
    $id = intval($_REQUEST['id']);
    $nilai_huruf = htmlspecialchars($_REQUEST['nilai_huruf']);
    if ($nilai_huruf == "A") $nilai_angka = "4";
    if ($nilai_huruf == "AB") $nilai_angka = "3.5";
    if ($nilai_huruf == "B") $nilai_angka = "3";
    if ($nilai_huruf == "BC") $nilai_angka = "2.5";
    if ($nilai_huruf == "C") $nilai_angka = "2";
    if ($nilai_huruf == "D") $nilai_angka = "1";
    if ($nilai_huruf == "E") $nilai_angka = "0";

    $sql = "update perkuliahan set nil_huruf='$nilai_huruf',nil_angka='$nilai_angka' where id='$id'";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'nil_huruf' => $nilai_huruf,
            'nil_angka' => $nilai_angka
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == "getmhs") {
  $sql = "SELECT nrp, nama from mahasiswa order by nrp";
  $rs = $conn->query($sql);
  $hasil = array();
  while ($row = $rs->fetch_assoc()) {
    array_push($hasil, $row);
  }
  echo json_encode($hasil);
}

$conn->close();
?>
