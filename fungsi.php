<?php
    function tanggalEnToId($tglEN) { // format MM/dd/yyyy
	$date = substr($tglEN,0,2);
	$month = substr($tglEN,3,2);
	$year = substr($tglEN,6,4);
	
	return $year."-".$month."-".$date;
}

function tanggalIdToEn($tglID) { // format yyyy-mm-dd
	$year = substr($tglID,0,4);
	$month = substr($tglID,5,2);
	$date = substr($tglID,8,2);
	
	return $month."/".$date."/".$year;
}
?>