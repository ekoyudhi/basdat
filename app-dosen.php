<?php
include 'conn.php';
include 'fungsi.php';

$action = $_GET['action'];

if ($action == 'get') {
    $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;
	$result = array();

	$rs = $conn->query("select count(*) from dosen");
	$row = $rs->fetch_row();
	$result["total"] = $row[0];
	$rs = $conn->query("select * from dosen limit $offset,$rows");
	$items = array();
	while($row = $rs->fetch_object()){
		array_push($items, $row);
	}
	$result["rows"] = $items;

	echo json_encode($result);
}
else if ($action == 'save') {
    $nip = htmlspecialchars($_REQUEST['nip']);
    $nama = htmlspecialchars($_REQUEST['nama']);

    
    $sql = "insert into dosen(nip,nama) values('$nip','$nama')";
    
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'nip' => $nip,
            'nama' => $nama
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == 'update') {
    $nip = htmlspecialchars($_REQUEST['nip']);
    $nama = htmlspecialchars($_REQUEST['nama']);
    
    $sql = "update dosen set nama='$nama' where nip=$nip";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'nip' => $nip,
            'nama' => $nama
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == 'destroy') {
    $nip = htmlspecialchars($_REQUEST['nip']);

	$rescek = $conn->query("select count(*) from mahasiswa where nip_wali='$nip'");
	$jmlcek = $rescek->fetch_row();
	if ($jmlcek[0] > 1) {
		echo json_encode(array('errorMsg'=>'Data dosen tidak dapat dihapus karena menjadi dosen wali'));
	}
	else {
		$sql = "delete from dosen where nip=$nip";
		
		$result = $conn->query($sql);
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
}

$conn->close();
?>