<?php
    include 'fungsi.php';
    include 'conn.php';

    $action = htmlspecialchars($_GET['action']);

    if ( $action == "getmatkul") {
        $ajaran = htmlspecialchars($_REQUEST['ajaran']);
        $rs = $conn->query("SELECT * FROM kelas WHERE frs='$ajaran'");
        $hasil = array();
        while($row = $rs->fetch_assoc()) {
            $r = array("id" => $row['kid'], "nama" => $row['kode']." | ".$row['nama']);
            array_push($hasil,$r);

        }
        header('Content-Type: application/json');
        echo json_encode($hasil);
    }
    else if ($action == "getnrp") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $rs = $conn->query("select mahasiswa.*, dosen.nama as nama_dosen from mahasiswa, dosen where mahasiswa.nip_wali = dosen.nip and mahasiswa.nrp='$nrp'");
        $total = $rs->num_rows;
        $hasil = array();
        while($row = $rs->fetch_assoc()) {
            array_push($hasil,$row);
        }
        header('Content-Type: application/json');
        echo json_encode(array("total" => $total,"rows"=>$hasil));
    }
    else if ($action == "getAllAjaran") {

    }
    else if ($action == "getfrs") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $periode = htmlspecialchars($_REQUEST['periode']);
        $tabel = "frs".substr($periode,0,2);
        //kid, kode matkul
        //$sql = "select a.id,a.kid, b.kode, b.nama, b.sks, b.kelas, a.nil_huruf from (select * from perkuliahan where nrp='$nrp' and frs='$periode') as a left join (select * from kelas) as b on a.kid=b.kid";
        $sql = "select a.id,a.kid, b.kode, b.nama, b.sks, b.kelas, a.nil_huruf from (select * from $tabel where nrp='$nrp' and frs='$periode') as a left join (select * from kelas) as b on a.kid=b.kid";
        $rs = $conn->query($sql);
        $result['total'] = $rs->num_rows;
        $items = array();
        $sks = 0;
        while($row = $rs->fetch_assoc()){
            array_push($items, $row);
            $sks += $row['sks'];
        }
        $result["rows"] = $items;
        $result["sks"] = $sks;

        echo json_encode($result);
    }
    else if ($action == "savefrs") {
        $nrp = htmlspecialchars($_REQUEST['frm_nrp']);
        $periode = htmlspecialchars($_REQUEST['frm_periode']);
        $kid = htmlspecialchars($_REQUEST['nama_matkul']);
        $tabel = "frs".substr($periode,0,2);

        //$sql = "INSERT INTO perkuliahan(frs,nrp,kid) VALUES ('$periode','$nrp','$kid')";
        $sql = "INSERT INTO $tabel(frs,nrp,kid) VALUES ('$periode','$nrp','$kid')";
        $result = $conn->query($sql);
        if ($result){
            echo json_encode(array(
                'nrp' => $nrp,
                'periode' => $periode,
                'kid' => $kid
            ));
        } else {
            echo json_encode(array('errorMsg'=> $conn->error));
        }
    }
    else if ($action == "hapusfrs") {
        $id = htmlspecialchars($_REQUEST['id']);
        $tahun = htmlspecialchars($_REQUEST['tahun']);
        $tabel = "frs".substr($tahun,2,2);

        //$sql = "delete from perkuliahan where id='$id'";
        $sql = "delete from $tabel where id='$id'";
        $result = $conn->query($sql);
        if ($result){
            echo json_encode(array('success'=>true));
        } else {
            echo json_encode(array('errorMsg'=>'Some errors occured.'));
        }
    }
    else if ($action == "getmhs") {
      $sql = "SELECT nrp, nama from mahasiswa order by nrp";
      $rs = $conn->query($sql);
      $hasil = array();
      while ($row = $rs->fetch_assoc()) {
        array_push($hasil, $row);
      }
      echo json_encode($hasil);
    }
    else if ($action == "cektabelfrs") {
      $tahun = htmlspecialchars($_REQUEST['tahun']);
      $th = substr($tahun,2,2);
      $sql = "SHOW TABLES LIKE 'frs$th'";
      $rs = $conn->query($sql);
      echo $rs->num_rows;
    }
    else if ($action == "buattabelfrs") {
      $tahun = htmlspecialchars($_REQUEST['tahun']);
      $th = substr($tahun,2,2);
      //$sql = "SHOW TABLES LIKE 'frs$th'";
      $sql = "CREATE TABLE frs$th LIKE frsstd";
      try {
        $rs = $conn->query($sql);
        echo "sukses";
      } catch (Exception $e) {
        echo "gagal";
      }
    }
    else if ($action == "gettabelfrs") {
      $sql = "SHOW TABLES LIKE 'frs%'";
      $rs = $conn->query($sql);
      $arr_thn = array();
      while ($row = $rs->fetch_row()) {
        if(strlen($row[0]) == 5) {
          $a = intval(substr($row[0],3,2));
          array_push($arr_thn, $a);
        }
      }
      $hasil = 2000 + max($arr_thn);
      echo $hasil;
    }
    $conn->close();

?>
