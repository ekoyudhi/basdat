<?php
include 'conn.php';
include 'fungsi.php';

$action = $_GET['action'];

if ($action == 'get') {
    $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;
	$result = array();

	$rs = $conn->query("select count(*) from ajaran");
	$row = $rs->fetch_row();
	$result["total"] = $row[0];
	$rs = $conn->query("select * from ajaran order by tahun desc, periode limit $offset,$rows");
	$items = array();
	while($row = $rs->fetch_object()){
		array_push($items, $row);
	}
	$result["rows"] = $items;

	echo json_encode($result);
}
else if ($action == 'save') {
    $frs = htmlspecialchars($_REQUEST['frs']);
    $semester = htmlspecialchars($_REQUEST['semester']);
	$tahun = htmlspecialchars($_REQUEST['tahun']);
	$periode = htmlspecialchars($_REQUEST['periode']);

    $sql = "insert into ajaran(frs,semester,tahun,periode) values('$frs','$semester','$tahun','$periode')";

    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'frs' => $frs,
            'semester' => $semester,
			'tahun' => $tahun,
			'periode' => $periode
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == 'update') {
    $frs = htmlspecialchars($_REQUEST['frs']);
    $semester = htmlspecialchars($_REQUEST['semester']);
	$tahun = htmlspecialchars($_REQUEST['tahun']);
	$periode = htmlspecialchars($_REQUEST['periode']);

    $sql = "update ajaran set semester='$semester', tahun='$tahun', periode='$periode' where frs=$frs";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'frs' => $frs,
            'semester' => $semester,
			'tahun' => $tahun,
			'periode' => $periode
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == 'destroy') {
    $frs = htmlspecialchars($_REQUEST['frs']);

	$rescek = $conn->query("select count(*) from mahasiswa where nip_wali='$nip'");
	$jmlcek = $rescek->fetch_row();
	if ($jmlcek[0] > 1) {
		echo json_encode(array('errorMsg'=>'Data dosen tidak dapat dihapus karena menjadi dosen wali'));
	}
	else {
		$sql = "delete from dosen where nip=$nip";

		$result = $conn->query($sql);
		if ($result){
			echo json_encode(array('success'=>true));
		} else {
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
}

$conn->close();
?>
