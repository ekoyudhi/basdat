<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Data Kelas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/easyui.css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="stylesheet" type="text/css" href="css/color.css">
	<link rel="stylesheet" type="text/css" href="css/demo.css">
	<link rel="stylesheet" type="text/css" href="css/apps.css">

	<script src="jss/jquery.min.js"></script>
	<script src="jss/bootstrap.min.js"></script>
	<script src="jss/bootbox.min.js"></script>
	<script type="text/javascript" src="jss/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="jss/apps.js"></script>
</head>
<body>
	<?php include "inc_nav.php"; ?>
	<h2>Data Kelas</h2>

	<table id="dg" title="Kelas" class="easyui-datagrid" style="width:100%;height:auto"
			url="app-kelas.php?action=get"
			toolbar="#toolbar" pagination="true"
			rownumbers="false" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
				<th field="kid" width="30" data-options="hidden:true">No</th>
				<th field="frs" width="50">Kode Ajaran</th>
				<th field="semester" width="140">Semester</th>
				<th field="rid" width="40">Kode Ruang</th>
				<th field="rnama" width="140">Nama Ruang</th>
				<th field="jam" width="50">Jam</th>
				<th field="kode" width="80">Kode Mata Kuliah</th>
				<th field="nama" width="180">Nama Mata Kuliah</th>
				<th field="sks" width="30">SKS</th>
				<th field="kelas" width="30">Kelas</th>
			</tr>
		</thead>
	</table>
	<div id="toolbar">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">Rekam</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Ubah</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Hapus</a>
	</div>

	<div id="dlg" class="easyui-dialog" style="width:auto;height:auto;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<div class="ftitle">Data Kelas</div>
		<form id="fm" method="post" novalidate>
			<div class="fitem">
				<label for="semester">Semester :</label>
				<input id= "semester" name="semester" class="easyui-combobox" required="true"
				data-options="valueField:'frs',
				textField:'semester',
				url:'app-kelas.php?action=getcombo&item=semester',
				onSelect: function(rec){
						var urlx = 'app-kelas.php?action=getcombo&item=matkul&periode='+rec.frs;
						$('#nama').combobox('clear');
						$('#nama').combobox('reload', urlx);
					}">
			</div>
			<div class="fitem">
				<label for="rnama">Ruang :</label>
				<input id= "rnama" name="rnama" class="easyui-combobox" required="true"
				data-options="valueField:'rid',textField:'rnama',url:'app-kelas.php?action=getcombo&item=ruang'">
			</div>
			<div class="fitem">
				<label for="nama">Mat Kul :</label>
				<!--input id= "nama" name="nama" class="easyui-combobox" required="true"
				data-options="valueField:'kode',textField:'nama',url:'app-kelas.php?action=getcombo&item=matkul'"-->
				<input id= "nama" name="nama" class="easyui-combobox" required="true" data-options="panelWidth:450,valueField:'nama',textField:'nama'">
			</div>
			<div class="fitem">
				<label for="jam">Jam :</label>
				<input id= "jam" name="jam" class="easyui-textbox" required="true">
			</div>
			<div class="fitem">
				<label for="kelas">Kelas :</label>
				<input id= "kelas" name="kelas" class="easyui-textbox">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
	</div>
	<script type="text/javascript">
		var url;
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Rekam');
			$('#fm').form('clear');
			url = 'app-kelas.php?action=save';
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','Ubah');
				//$('#fm').form('load',row);
				$('#semester').combobox('select', row.semester);
				url = 'app-kelas.php?action=update&nip='+row.kid;
			}
		}
		function saveUser(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlg').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){

			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Konfirmasi','Apakah anda akan menghapus data kelas '+row.kid+' ?.',function(r){
					if (r){
						$.post('app-kelas.php?action=destroy',{kid:row.kid},function(result){
							if (result.success){
								$('#dg').datagrid('reload');	// reload the user data
							} else {
								/*
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
								*/
								$.messager.alert('Error',result.errorMsg,'error');
							}
						},'json');
					}
				});
			}
		}
	</script>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
</body>
</html>
