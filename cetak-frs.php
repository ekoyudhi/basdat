<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Cetak FRS</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!--link rel="stylesheet" type="text/css" href="css/easyui.css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="stylesheet" type="text/css" href="css/color.css">
	<link rel="stylesheet" type="text/css" href="css/demo.css">
	<link rel="stylesheet" type="text/css" href="css/apps.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"--    >

	<script src="jss/jquery.min.js"></script>
	<script src="jss/bootstrap.min.js"></script>
	<script src="jss/bootbox.min.js"></script>
	<!--script type="text/javascript" src="jss/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="jss/apps.js"></script>
	<script type="text/javascript" src="jss/jquery.dataTables.min.js"></script-->
</head>
<body>
	<?php
	include "conn.php";
	$nrp = isset($_GET['nrp']) ? $_GET['nrp'] : "";
	$periode = isset($_GET['periode']) ? $_GET['periode'] : "";
	if ($nrp == "" || $periode == "") {
		echo "kosong";
	}
	else {
		$sem = substr($periode,3,1);
		$th = substr($periode,0,2);
		$thn = 2000 + intval($th);
		$smtr = $sem == "1" ? "Gasal" : "Genap";
		$sql = "select a.id,a.kid, b.kode, b.nama, b.sks, b.kelas, a.nil_huruf from (select * from perkuliahan where nrp='$nrp' and frs='$periode') as a left join (select * from kelas) as b on a.kid=b.kid";
		$rs = $conn->query($sql);
		$result['total'] = $rs->num_rows;
		$items = array();
		$sks = 0;
		while($row = $rs->fetch_assoc()){
				array_push($items, $row);
				$sks += $row['sks'];
		}
	?>
	<div class="text-center"><h3>Formulir Rencana Studi Mahasiswa</h3></div>
	<div class="text-center"><h5>Periode Semester <?php echo $smtr; ?> Tahun Akademik <?php echo $thn."/".($thn+1); ?></h5></div>
	<div align="center">
		<table class="table table-bordered" width="400">
			<thead>
				<tr><th colspan="2" class="text-center">Mahasiswa</th></tr>
			</thead>
			<tbody>
				<tr><td>NRP</td><td>07111750067001</td></tr>
				<tr><td>Nama</td><td>Eko Yudhi Prastowo</td></tr>
			</tbody>
		</table>
	</div>
<table class="table table-bordered">
	<thead>
		<tr><th colspan="3" class="text-center">Mata Kuliah Diambil</th></tr>
		<tr><th width="10">No</th><th>Mata Kuliah</th><th>SKS</th></tr>
	</thead>
	<tbody>
		<?php
		 	$no = 0;
			foreach($items as $row) {
				$no++;
				echo "<tr><td>".$no."</td><td>".$row['kode']." - ".$row['nama']."</td><td>".$row['sks']."</tr>";
			}
		 ?>
		 <tr><th colspan="2" class="text-center">Total SKS</th><th><?php echo $sks; ?></th></tr>
	</tbody>
</table>
<table id="setuju" class="table">
	<tbody>
		<tr><td>Persetujuan Dosen Wali</td></tr>
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr><td><strong>Dr. Adhi Dharma Wibawa, ST, MT</strong></td></tr>
		<tr><td><strong>NIP 1300001</strong></td></tr>
	</tbody>
</table>
<?php }
$conn->close()
 ?>
 <style>
 .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
	 padding: 6px;
 }
 .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
	 border-top-width: 1px;
	 border-bottom-width: 0px;
 }
 .table-bordered tbody tr td,
 .table-bordered tbody tr th,
 .table-bordered tfoot tr td,
 .table-bordered tfoot tr th,
 .table-bordered thead tr td,
 .table-bordered thead tr th {
	 border: 1px solid #000;
 }
 	#setuju>tbody>tr>td {
		border-top: 1px solid #fff;
	}
	body {
    font-family:verdana,helvetica,arial,sans-serif;
    padding:20px;
    font-size:12px;
    margin:0;
}
.demo-info{
	padding:0 0 12px 0;
}
.demo-tip{
	display:none;
}
.label-top{
    display: block;
    height: 22px;
    line-height: 22px;
    vertical-align: middle;
}
 </style>
</body>
</html>
