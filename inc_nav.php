<style>
.dropdown-submenu {
    position: relative;
}

.dropdown-submenu .dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -1px;
}
</style>
<script type="text/javascript" src="jss/js.cookie.js"></script>
<nav class="navbar navbar-default" data-spy="affix" data-offset-top="197">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="javascript:void(0)">Sistem Kemahasiswaan</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="" id="menu_home"><a href="/basdat/">Home</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">Data <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="view-mahasiswa.php" id="menu_mhs">Mahasiswa</a></li>
          <li><a href="view-dosen.php" id="menu_dosen">Dosen</a></li>
          <li><a href="view-ruang.php" id="menu_ruang">Ruang Kuliah</a></li>
          <li><a href="view-kurikulum.php" id="menu_kurikulum">Kurikulum</a></li>
          <li><a href="view-matkul.php" id="menu_matkul">Mata Kuliah</a></li>
          <li><a href="view-ajaran.php" id="menu_ajaran">Ajaran</a></li>
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">Akademik <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="view-kelas.php" id="menu_kelas">Kelas</a></li>
          <li><a href="view-frs-uas.php" id="menu_frs">Formulir Rencana Studi</a></li>
          <li><a href="view-nilai.php" id="menu_nilai">Nilai Mahasiswa</a></li>
          <li><a href="view-histori.php" id="menu_histori">Histori</a></li>
          <li class="dropdown-submenu"><a class="test" href="javascript:void(0)">Transkrip <span class="caret"></a></span>
            <ul class="dropdown-menu">
              <li><a href="view-transkrip-semester.php">Transkrip Semester</a></li>
              <li><a href="view-transkrip-final.php">Transkrip Akademik</a></li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>

    <div class="navbar-form navbar-right">
      <div class="form-group">
        Tahun Akademik :
        <select id="thn_akademik" class="form-control">
          <option value="2019">2019</option>
          <option value="2018">2018</option>
          <option value="2017">2017</option>
          <option value="2016">2016</option>
          <option value="2015">2015</option>
        </select>
      </div>
      <button id="btn_thn_akademik" type="submit" class="btn btn-default">Ganti</button>
    </div>
  </div>
</nav>
<div style="height: 30px" align="right">
  <span class="badge">Anda sedang membuka Tahun Akademik <?php echo $_COOKIE['thn']; ?></span>
</div>
<script>
$(document).ready(function(){

  var th = Cookies.get('thn');
  if (th != undefined) {
    //alert(th);
    $("#thn_akademik").val(th);
  } else {
    $.get("app-frs-uas.php?action=gettabelfrs", function(data){
      $("#thn_akademik").val(data);
      $('#btn_thn_akademik').click();
    });
  }

  $('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });

  $('#btn_thn_akademik').on("click", function() {
    var thn = $('#thn_akademik').val();

    //location.reload();
    $.get("app-frs-uas.php?action=cektabelfrs&tahun="+thn, function(data){
      //alert("Data: " + data);

      if (data == 0) {
        /*
        $.messager.confirm('Peringatan', 'Tabel FRS Tahun Akademik '+thn+' belum ada. Apakah anda akan membuat tabel FRS'+thn.substring(2,4)+'?', function(r){
            if (r){
                //alert('confirmed: '+r);
                $.get("app-frs-uas.php?action=buattabelfrs&tahun="+thn, function(data){
                  alert(data);
                  Cookies.set('thn', thn, { expires: 7 });
                  location.reload();
                });
            } else {
              location.reload();
            }
        });
        */
        $.messager.confirm({
          closable: false,
          title: 'Peringatan',
        	msg: 'Tabel FRS Tahun Akademik '+thn+' belum ada. Apakah anda akan membuat tabel FRS'+thn.substring(2,4)+'?',
          ok: 'Ya',
          cancel: 'Tidak',
        	fn: function(r){
        		if (r){
        			//alert('confirmed: '+r);
              $.get("app-frs-uas.php?action=buattabelfrs&tahun="+thn, function(data){
                alert(data);
                Cookies.set('thn', thn, { expires: 7 });
                location.reload();
              });
        		}
            else {
              location.reload();
            }
        	}
        });
      } else {
        Cookies.set('thn', thn, { expires: 7 });
        location.reload();
      }
      //location.reload();
    });

  });
});
</script>
