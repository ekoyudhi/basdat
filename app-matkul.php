<?php
include 'conn.php';
include 'fungsi.php';

$action = $_GET['action'];

if ($action == 'get') {
    $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;
	$result = array();

	$rs = $conn->query("select count(*) from kuliah");
	$row = $rs->fetch_row();
	$result["total"] = $row[0];
	$rs = $conn->query("select * from kuliah limit $offset,$rows");
	$items = array();
	while($row = $rs->fetch_object()){
		array_push($items, $row);
	}
	$result["rows"] = $items;

	echo json_encode($result);
}
else if ($action == 'save') {
    $kode = htmlspecialchars($_REQUEST['kode']);
    $nama = htmlspecialchars($_REQUEST['nama']);
	$sks = htmlspecialchars($_REQUEST['sks']);
    
    $sql = "insert into kuliah(kode,nama,sks) values('$kode','$nama','$sks')";
    
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'kode' => $kode,
            'nama' => $nama,
			'sks' => $sks
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == 'update') {
    $kode = htmlspecialchars($_REQUEST['kode']);
    $nama = htmlspecialchars($_REQUEST['nama']);
	$sks = htmlspecialchars($_REQUEST['sks']);
    
    $sql = "update kuliah set nama='$nama', sks='$sks' where kode='$kode'";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'kode' => $kode,
            'nama' => $nama,
			'sks' => $sks
        ));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}
else if ($action == 'destroy') {
    $kode = htmlspecialchars($_REQUEST['kode']);

    $sql = "delete from kuliah where kode='$kode'";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array('success'=>true));
    } else {
        echo json_encode(array('errorMsg'=>'Some errors occured.'));
    }
}

$conn->close();
?>