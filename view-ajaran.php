<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Data Ajaran</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/easyui.css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="stylesheet" type="text/css" href="css/color.css">
	<link rel="stylesheet" type="text/css" href="css/demo.css">
	<link rel="stylesheet" type="text/css" href="css/apps.css">

	<script src="jss/jquery.min.js"></script>
	<script src="jss/bootstrap.min.js"></script>
	<script type="text/javascript" src="jss/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="jss/apps.js"></script>
</head>
<body>
	<?php include "inc_nav.php"; ?>
	<h2>Data Ajaran</h2>

	<table id="dg" title="Ajaran" class="easyui-datagrid" style="width:100%;height:auto"
			url="app-ajaran.php?action=get"
			toolbar="#toolbar" pagination="true" remoteFilter="true"
			rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
				<th field="frs" width="50">Kode</th>
				<th field="semester" width="100">Semester</th>
				<th field="tahun" width="100">Tahun</th>
				<th field="periode" width="100">Periode</th>
			</tr>
		</thead>
	</table>
	<div id="toolbar">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">Rekam</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Ubah</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Hapus</a>
	</div>

	<div id="dlg" class="easyui-dialog" style="width:auto;height:auto;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<div class="ftitle">Data Ajaran</div>
		<form id="fm" method="post" novalidate>
			<div class="fitem">
				<label for="frs">Kode :</label>
				<input id= "frs" name="frs" class="easyui-validatebox" required="true" maxlength="4">
			</div>
			<div class="fitem">
				<label for="semester">Semester :</label>
				<input id="semester" name="semester" class="easyui-textbox" required="true">
			</div>
			<div class="fitem">
				<label for="tahun">Tahun :</label>
				<input id="tahun" name="tahun" class="easyui-textbox" required="true">
			</div>
			<div class="fitem">
				<label for="periode">Periode :</label>
				<input id="periode" name="periode" class="easyui-textbox" required="true">
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser()" style="width:90px">Save</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
	</div>
	<script type="text/javascript">
		var url;
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Rekam');
			$('#fm').form('clear');
			//$("#nip").prop('disabled', false);
			url = 'app-ajaran.php?action=save';
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','Ubah');
				$('#fm').form('load',row);
				//$("#nip").prop('disabled', true);
				url = 'app-ajaran.php?action=update&frs='+row.frs;
			}
		}
		function saveUser(){
			$('#fm').form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						$('#dlg').dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Konfirmasi','Apakah anda akan menghapus data dosen NIP '+row.nip+' ?.',function(r){
					if (r){
						$.post('app-ajaran.php?action=destroy',{frs:row.frs},function(result){
							if (result.success){
								$('#dg').datagrid('reload');	// reload the user data
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							}
						},'json');
					}
				});
			}
		}
	</script>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
</body>
</html>
