<?php
include 'conn.php';
include 'fungsi.php';

$action = $_GET['action'];

    if ( $action == "getmatkul") {
        $ajaran = htmlspecialchars($_REQUEST['ajaran']);
        $rs = $conn->query("SELECT * FROM kelas WHERE frs='$ajaran'");
        $hasil = array();
        while($row = $rs->fetch_assoc()) {
            $r = array("id" => $row['kid'], "nama" => $row['kode']." | ".$row['nama']);
            array_push($hasil,$r);

        }
        header('Content-Type: application/json');
        echo json_encode($hasil);
    } else if ($action == "getnrp") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $rs = $conn->query("select mahasiswa.*, dosen.nama as nama_dosen from mahasiswa, dosen where mahasiswa.nip_wali = dosen.nip and mahasiswa.nrp='$nrp'");
        $total = $rs->num_rows;
        $hasil = array();
        while($row = $rs->fetch_assoc()) {
            array_push($hasil,$row);
        }
        header('Content-Type: application/json');
        echo json_encode(array("total" => $total,"rows"=>$hasil));
    }
    else if ($action == "getfrs") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);

        $sql = "select x.kode, max(frs) as frs
        from (select a.id,a.kid,a.frs, b.kode, b.nama, b.sks, b.kelas, a.nil_huruf, a.nil_angka from perkuliahan a
        left join kelas b on a.kid=b.kid
        where nil_huruf is not null and nrp='$nrp') as x group by kode";

        $sql2 = "select a.id,a.kid,a.frs, b.kode, b.nama, b.sks, b.kelas, a.nil_huruf, a.nil_angka
                from perkuliahan a left join kelas b on a.kid=b.kid
                Where nrp='$nrp' order by kode";

        $rs = $conn->query($sql);
        $rs2 = $conn->query($sql2);
        $arr1 = array();
        $arr2 = array();
        while ($row = $rs->fetch_assoc()) {
          array_push($arr1, $row);
        }
        while ($row = $rs2->fetch_assoc()) {
          foreach ($arr1 as $s) {
            if (($row['kode'] == $s['kode']) && ($row['frs'] == $s['frs'])) {
              array_push($arr2,$row);
            }
          }
        }

        $all = array();
        $nilai_mutu = 0;
        $sks = 0;
        $ipk = 0;
        foreach($arr2 as $t) {
          $nilai_mutu += (doubleval($t['nil_angka']) * doubleval($t['sks']));
          $sks += intval($t['sks']);
          array_push($all,$t);
        }
        if ($sks == 0 && $nilai_mutu == 0) {
          $ipk == 0;
        } else {
          $ipk = $nilai_mutu / $sks;
        }
        Header('Content-Type: application/json');
        $result['total'] = count($all);
        $result['rows'] = $all;
        $result['sks'] = $sks;
        $result['ipk'] = number_format($ipk,2,',','.');
        echo json_encode($result);
    }
    else if ($action == "getfrs1") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);

        $sql = "select x.kode, max(x.frs) as frs from (select c.* from (select * from all_frs where nrp='$nrp') c
        inner join (select kode, max(nil_angka) m from (select * from all_frs where nrp='$nrp') d group by kode) e
        on c.kode = e.kode
        and c.nil_angka = e.m order by kode) x group by kode";

        $sql2 = "select c.* from (select * from all_frs where nrp='$nrp') c
        inner join (select kode, max(nil_angka) m from (select * from all_frs where nrp='$nrp') d group by kode) e
        on c.kode = e.kode
        and c.nil_angka = e.m order by kode";

        $rs = $conn->query($sql);
        $rs2 = $conn->query($sql2);
        $arr1 = array();
        $arr2 = array();
        while ($row = $rs->fetch_assoc()) {
          array_push($arr1, $row);
        }
        while ($row = $rs2->fetch_assoc()) {
          foreach ($arr1 as $s) {
            if (($row['kode'] == $s['kode']) && ($row['frs'] == $s['frs'])) {
              array_push($arr2,$row);
            }
          }
        }

        $all = array();
        $nilai_mutu = 0;
        $sks = 0;
        $ipk = 0;
        foreach($arr2 as $t) {
          $nilai_mutu += (doubleval($t['nil_angka']) * doubleval($t['sks']));
          $sks += intval($t['sks']);
          array_push($all,$t);
        }
        if ($sks == 0 && $nilai_mutu == 0) {
          $ipk == 0;
        } else {
          $ipk = $nilai_mutu / $sks;
        }
        Header('Content-Type: application/json');
        $result['total'] = count($all);
        $result['rows'] = $all;
        $result['sks'] = $sks;
        $result['ipk'] = number_format($ipk,2,',','.');
        echo json_encode($result);
    }
else if ($action == "getmhs") {
  $sql = "SELECT nrp, nama from mahasiswa order by nrp";
  $rs = $conn->query($sql);
  $hasil = array();
  while ($row = $rs->fetch_assoc()) {
    array_push($hasil, $row);
  }
  echo json_encode($hasil);
}

$conn->close();
?>
