<?php
    include 'fungsi.php';
    include 'conn.php';
    include("class.simtool.php");
    
    
    $action = $_GET['action'];
    
    if ($action == "get") {
        
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $offset = ($page-1)*$rows;
        
        $result = array();
    
        $rs = $conn->query("select count(*) from mahasiswa");
        $row = $rs->fetch_row();
        $result["total"] = $row[0];
        $rs = $conn->query("select mahasiswa.nrp, mahasiswa.nama, mahasiswa.alamat, mahasiswa.gaji, date_format(mahasiswa.tglahir,'%d-%m-%Y') as tglahir, dosen.nama as nama_dosen from mahasiswa, dosen where mahasiswa.nip_wali = dosen.nip order by mahasiswa.nrp limit $offset,$rows");
        $items = array();
        while($row = $rs->fetch_object()){
            array_push($items, $row);
        }
        $result["rows"] = $items;
    
        echo json_encode($result);
        //print_r($result);
    }
    else if ($action == "save") {
        
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $nama = htmlspecialchars($_REQUEST['nama']);
        $alamat = htmlspecialchars($_REQUEST['alamat']);
        $tglahir= tanggalEnToId(htmlspecialchars($_REQUEST['tglahir']));
        $gaji = htmlspecialchars($_REQUEST['gaji']);
        $nama_dosen = htmlspecialchars($_REQUEST['nama_dosen']);
        
        $sql = "insert into mahasiswa(nrp,nama,alamat,tglahir,gaji,nip_wali) values('$nrp','$nama','$alamat','$tglahir','$gaji','$nama_dosen')";
        //$result = @mysql_query($sql);
        $result = $conn->query($sql);
        if ($result){
            echo json_encode(array(
                'nrp' => $nrp,
                'nama' => $nama,
                'alamat' => $alamat,
                'tglahir' => $tglahir,
                'gaji' => $gaji,
                'nip_wali' => $nama_dosen
            ));
        } else {
            echo json_encode(array('errorMsg'=>'Some errors occured.'));
        }
    }
    else if ($action == "update") {
        
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $nama = htmlspecialchars($_REQUEST['nama']);
        $alamat = htmlspecialchars($_REQUEST['alamat']);
        $gaji = htmlspecialchars($_REQUEST['gaji']);
        $tglahir = tanggalEnToId(htmlspecialchars($_REQUEST['tglahir']));
        $nama_dosen = htmlspecialchars($_REQUEST['nama_dosen']);

        $res = $conn->query("select dosen.nama as nama_dosen from mahasiswa, dosen where mahasiswa.nip_wali = dosen.nip and mahasiswa.nrp = '$nrp'");
        $row_dos = $res->fetch_row();
        if ($row_dos[0] == $nama_dosen) {
            $sql = "update mahasiswa set nama='$nama',alamat='$alamat',gaji='$gaji',tglahir='$tglahir' where nrp=$nrp";
        } else {
            $sql = "update mahasiswa set nama='$nama',alamat='$alamat',gaji='$gaji',tglahir='$tglahir',nip_wali='$nama_dosen' where nrp=$nrp";
        }
        $result = $conn->query($sql);
        if ($result){
            echo json_encode(array(
                'nrp' => $nrp,
                'nama' => $nama,
                'alamat' => $alamat,
                'gaji' => $gaji,
                'tglahir' => $tglahir,
                'nip_wali' => $nama_dosen
            ));
        } else {
            echo json_encode(array('errorMsg'=>'Some errors occured.'));
        }
    }
    else if ($action == "destroy") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);

        $sql = "delete from mahasiswa where nrp=$nrp";
        $result = $conn->query($sql);
        if ($result){
            echo json_encode(array('success'=>true));
        } else {
            echo json_encode(array('errorMsg'=>'Some errors occured.'));
        }
    }
    else if ($action == "getdosen") {
        $result = $conn->query("select nip, nama from dosen");
        
        $items = array();
        while($row = $result->fetch_object()){
            array_push($items,$row);
        }
        
        echo json_encode($items);
    }
    else if ($action == "carix") {
        //$nrp = htmlspecialchars($_POST['nrp']);
        //$nama = htmlspecialchars($_POST['nama']);
        //echo json_encode(array("aaa","bbb"));
    }
    else if ($action == "cari") {
        
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $offset = ($page-1)*$rows;
        $result = array();
        
        $nrp = htmlspecialchars($_REQUEST['cari_nrp']);
        $nama = htmlspecialchars($_REQUEST['cari_nama']);
        
        $script = "";
        if ($nrp != "") {
            $script = $script." and mahasiswa.nrp like '%".$nrp."%' ";
        }
        
        if ($nama != "") {
            $script = $script." and mahasiswa.nama like '%".$nama."%' ";
        }
        $script_count = "";
        $where_count = " where ";
        
        if (strlen($script) > 0) {
            $script_count = substr($script,4,strlen($script)-1);
        } else {
            $where_count = "";
        }
        
        $rs = $conn->query("select count(*) from mahasiswa ".$where_count.$script_count."");
        $row = $rs->fetch_row();
        $result["total"] = $row[0];
        $rs = $conn->query("select mahasiswa.nrp, mahasiswa.nama, mahasiswa.alamat, mahasiswa.gaji, date_format(mahasiswa.tglahir,'%d-%m-%Y') as tglahir, dosen.nama as nama_dosen from mahasiswa, dosen where mahasiswa.nip_wali = dosen.nip ".$script." order by mahasiswa.nrp limit $offset,$rows");
        $items = array();
        while($row = $rs->fetch_object()){
            array_push($items, $row);
        }
        $result["rows"] = $items;
        
        if ($result['total'] > 0) {
            echo json_encode($result);
        }
        else {
            /* METODE SIMILARITY*/
            $simTool = new LetterPairSimilarity;
            $resSim = $conn->query("select mahasiswa.nrp, mahasiswa.nama, mahasiswa.alamat, mahasiswa.gaji, date_format(mahasiswa.tglahir,'%d-%m-%Y') as tglahir, dosen.nama as nama_dosen from mahasiswa, dosen where mahasiswa.nip_wali = dosen.nip order by mahasiswa.nrp");
            //$resSim = $conn->query("select * from mahasiswa");
            $arrSim = array();
            $arrAll = array();
            while($rowSim = $resSim->fetch_assoc()) {
                $arr = array('nrp' => $rowSim['nrp'], 'nama' => $rowSim['nama'], 'alltext' => $rowSim['nrp'].$rowSim['nama']);
                array_push($arrSim,$arr);
                array_push($arrAll,$rowSim);
            }
            
            $hasil = array();
            $hasil['nilai'] = 0;
            $hasil['nrp'] = "";
            $hasil['nama'] = "";
            if ($nrp!="" && $nama!="") {
                foreach($arrSim as $rowAll) {
                    $intCompare = $simTool->compareStrings($nrp.$nama,$rowAll['alltext']);
                    if ($intCompare > $hasil['nilai']) {
                        $hasil['nilai'] = $intCompare;
                        $hasil['nrp'] = $rowAll['nrp'];
                        $hasil['nama'] = $rowAll['nama'];
                    }
                }
            }
            else if ($nrp!="" && $nama=="") {
                
            }
            else if ($nrp=="" && $nama!="") {
                foreach($arrSim as $rowAll) {
                    $intCompare = $simTool->compareStrings($nama,$rowAll['nama']);
                    if ($intCompare > $hasil['nilai']) {
                        $hasil['nilai'] = $intCompare;
                        $hasil['nrp'] = $rowAll['nrp'];
                        $hasil['nama'] = $rowAll['nama'];
                    }
                }
            }
            if ($hasil['nilai'] > 0) {
                $arrHasil = array();
                foreach($arrAll as $r2) {
                    if ($r2['nrp'] == $hasil['nrp']) {
                        array_push($arrHasil,$r2);
                    }
                }
                $result['total'] = count($arrHasil);
                $result['rows'] = $arrHasil;
                
                echo json_encode($result);
                /* AKHIR METODE SIMILARITY */
            }
            else
            {
                $arrHuruf = array('A'=>1,'B'=>2,'C'=>3,'D'=>4,'E'=>5,'F'=>6,'G'=>7,'H'=>8,'I'=>9,'J'=>10,
                  'K'=>11,'L'=>12,'M'=>13,'N'=>14,'O'=>15,'P'=>16,'Q'=>17,'R'=>18,'S'=>19,
                  'T'=>20,'U'=>21,'V'=>22,'W'=>23,'X'=>24,'Y'=>25,'Z'=>26);
                
                $beda = 26;
                
                if ($nrp != "" && $nama!= "") {
                    foreach($arrAll as $r) {
                        $idxCari = $arrHuruf[strtoupper(substr($nama,0,1))];
                        $idxHasil = $arrHuruf[strtoupper(substr($r['nama'],0,1))];
                        
                        if (abs($idxCari - $idxHasil) < $beda) {
                            $beda = abs($idxCari - $idxHasil);
                            $hasil = $r;
                        }
                        
                    }
                } else if ($nrp=="" && $nama != "") {
                    foreach($arrAll as $r) {
                        $idxCari = $arrHuruf[strtoupper(substr($nama,0,1))];
                        $idxHasil = $arrHuruf[strtoupper(substr($r['nama'],0,1))];
                        
                        if (abs($idxCari - $idxHasil) < $beda) {
                            $beda = abs($idxCari - $idxHasil);
                            $hasil = $r;
                        }
                        
                    }
                } else if ($nrp != "" && $nama=="") {
                    $selisih = 99999;
                    foreach($arrAll as $r) {
                        if (abs(doubleval($nrp) - doubleval($r['nrp'])) < $selisih) {
                            $selisih = abs(doubleval($nrp) - doubleval($r['nrp']));
                            $hasil = $r;
                        }
                    }
                }
                $result['total'] = 1;
                $result['rows'] = array($hasil);
                
                echo json_encode($result);
                
            }
            
        }
        
        
    }
    
    else if ($action == "getnrp") {
        $nrp = $_REQUEST['nrp'];
        $rs = $conn->query("SELECT * FROM mahasiswa WHERE nrp='$nrp'");
        $xx = json_encode(array("jumlah" => $rs->num_rows));
        header('Content-Type: application/json');
        echo $xx;
    }
    else if ($action == "getlock") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $sql = "SELECT flag FROM mahasiswa WHERE nrp=$nrp";
        $result = $conn->query($sql);
        $flag = $result->fetch_assoc();
        header('Content-Type: application/json');
        echo json_encode(array('condition'=>$flag['flag']));
    }
    else if ($action == "lock") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $sql = "UPDATE mahasiswa SET flag='1' WHERE nrp=$nrp";
        $result = $conn->query($sql);
        if ($result){
            echo json_encode(array('success'=>true));
        } else {
            echo json_encode(array('errorMsg'=> $conn->error));
        }
    }
    else if ($action == "unlock") {
        $nrp = htmlspecialchars($_REQUEST['nrp']);
        $sql = "UPDATE mahasiswa SET flag='0' WHERE nrp='$nrp'";
        $result = $conn->query($sql);
        if ($result){
            echo json_encode(array('success'=>true));
        } else {
            echo json_encode(array('errorMsg'=> $conn->error));
        }
    }
    $conn->close();
?>