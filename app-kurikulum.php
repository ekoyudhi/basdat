<?php
include 'conn.php';
include 'fungsi.php';

$action = $_GET['action'];

if ($action == 'get') {
    $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;
	$result = array();

	$rs = $conn->query("select count(*) from kurikulum");
	$row = $rs->fetch_row();
	$result["total"] = $row[0];
	$rs = $conn->query("select * from kurikulum order by tahun desc, semester, kode limit $offset,$rows");
	$items = array();
	while($row = $rs->fetch_object()){
		array_push($items, $row);
	}
	$result["rows"] = $items;

	echo json_encode($result);
}
else if ($action == 'save') {
    $kode = htmlspecialchars($_REQUEST['kode']);
    $tahun = htmlspecialchars($_REQUEST['tahun']);
    $semester = htmlspecialchars($_REQUEST['semester']);
    $nama = htmlspecialchars($_REQUEST['nama']);
    $sks = htmlspecialchars($_REQUEST['sks']);

    $sql = "insert into kurikulum(tahun,semester,kode,nama,sks) values('$tahun','$semester','$kode','$nama','$sks')";

    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
            'tahun' => $tahun,
            'semester' => $semester,
            'kode' => $kode,
            'nama' => $nama,
            'sks' => $sks
        ));
    } else {
        echo json_encode(array('errorMsg'=> $conn->error));
    }
}
else if ($action == 'update') {
    $kode = htmlspecialchars($_REQUEST['kode']);
    $id = htmlspecialchars($_REQUEST['id']);
    $tahun = htmlspecialchars($_REQUEST['tahun']);
    $semester = htmlspecialchars($_REQUEST['semester']);
    $nama = htmlspecialchars($_REQUEST['nama']);
    $sks = htmlspecialchars($_REQUEST['sks']);

    $sql = "update kurikulum set tahun='$tahun', semester='$semester', kode='$kode', nama='$nama', sks='$sks' where id='$id'";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array(
          'id' => $id,
          'tahun' => $tahun,
          'semester' => $semester,
          'kode' => $kode,
          'nama' => $nama,
          'sks' => $sks
        ));
    } else {
        echo json_encode(array('errorMsg'=> $conn->error));
    }
}
else if ($action == 'destroy') {
    $id = htmlspecialchars($_REQUEST['id']);

    $sql = "delete from kurikulum where id='$id'";
    $result = $conn->query($sql);
    if ($result){
        echo json_encode(array('success'=>true));
    } else {
        echo json_encode(array('errorMsg'=> $conn->error));
    }
}

$conn->close();
?>
