<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Sistem Kemahasiswaan</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/easyui.css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="stylesheet" type="text/css" href="css/color.css">
	<link rel="stylesheet" type="text/css" href="css/demo.css">
	<link rel="stylesheet" type="text/css" href="css/apps.css">

	<!--script type="text/javascript" src="jss/jquery-1.6.min.js"></script-->
	<script src="jss/jquery.min.js"></script>
	<script src="jss/bootstrap.min.js"></script>
	<script type="text/javascript" src="jss/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="jss/apps.js"></script>
</head>
<body>
	<?php include "inc_nav.php"; ?>
	<div class="container">
	<div class="jumbotron">
		<h1>Sistem Kemahasiswaan</h1>
		<p>Mata Kuliah Sistem Manajemen Basis Data Terdistribusi</p>
		<p>KELOMPOK 6</p>
		<p>
		1. Eko Yudhi Prastowo (07111750067001)<br>
		2. Dwi Rahmat Mulyanto (07111750067016)<br>
		3. Suwander Husada (07111750067005)</p>
	</div>
	</div>
</body>
</html>
